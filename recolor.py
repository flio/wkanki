#!/usr/bin/env python3

from xml.dom import minidom
from os import listdir, mkdir
from os.path import isfile, join, isdir
from svgpathtools import svg2paths


def recolor_svg(dom, path_lengths):
    svg = dom.getElementsByTagName("svg")[0]
    groups = [
        e for e in svg.childNodes if e.nodeType == e.ELEMENT_NODE and e.tagName == "g"
    ]
    strokes = groups[0]
    numbers = groups[1]

    numbers.setAttribute("font-size", "8")
    numbers.setAttribute("fill", "#808080")

    kanji = strokes.getElementsByTagName("g")[0].getAttribute("kvg:element")

    # Remove all the IDs since we're going to duplicate the strokes. Also remove
    # KVG attributes that we don't use to save some space
    for e in svg.getElementsByTagName("g") + svg.getElementsByTagName("path"):
        for attr in [
            "id",
            "xmlns:kvg",
            "kvg:type",
            "kvg:variant",
            "kvg:original",
            "kvg:phon",
            "kvg:position",
            "kvg:radical",
            "style",
        ]:
            if e.hasAttribute(attr):
                e.removeAttribute(attr)

    # Remove SVG width/height to make it easier to resize it
    svg.removeAttribute("width")
    svg.removeAttribute("height")

    (w, h) = [int(d) for d in svg.getAttribute("viewBox").split()[2:4]]

    # Add grid
    grid = dom.createElement("g")
    grid.setAttribute("stroke", "#8080a0")
    grid.setAttribute("stroke-width", "0.5")
    grid.setAttribute("opacity", "0.7")
    for (x1, y1), (x2, y2) in [
        ((0, 0), (w, 0)),
        ((w, 0), (w, h)),
        ((0, h), (w, h)),
        ((0, 0), (0, h)),
        ((0, h / 2), (w, h / 2)),
        ((w / 2, 0), (w / 2, h)),
    ]:
        line = dom.createElement("line")
        line.setAttribute("x1", str(x1))
        line.setAttribute("y1", str(y1))
        line.setAttribute("x2", str(x2))
        line.setAttribute("y2", str(y2))
        grid.appendChild(line)

    svg.insertBefore(grid, strokes)

    if strokes.hasAttribute("style"):
        strokes.removeAttribute("style")
    strokes.setAttribute("fill", "none")
    strokes.setAttribute("stroke-linecap", "round")
    strokes.setAttribute("stroke-linejoin", "round")

    # Clone the strokes for animation
    animated_strokes = strokes.cloneNode(deep=True)
    svg.insertBefore(animated_strokes, numbers)

    strokes.setAttribute("stroke", "#808080")
    strokes.setAttribute("opacity", "0.5")
    strokes.setAttribute("stroke-width", "3")

    animated_strokes.setAttribute("stroke", "#c04080")
    animated_strokes.setAttribute("stroke-width", "2")

    # Generate a unique id for the animation in case we want to include several
    # chars on the same page, this is used to reset the animation on click
    aid = "aid{:05x}".format(ord(kanji))
    svg.setAttribute("id", aid)

    # Animate the paths
    wait = 0.2
    draw_time = 0.3
    length_factor = 0.005
    wait_before_repeat = 2
    total_len = (wait + draw_time) * len(path_lengths) + wait_before_repeat
    total_len = total_len + length_factor * sum(path_lengths)
    acc_len = 0.0
    for path, number, l in zip(
        animated_strokes.getElementsByTagName("path"),
        numbers.getElementsByTagName("text"),
        path_lengths,
    ):
        # Animate the number
        anim = dom.createElement("animate")
        anim.setAttribute("attributeName", "opacity")
        anim.setAttribute("values", "0.2;0.2;1.0;1.0")
        anim.setAttribute(
            "keyTimes", "0;%.4f;%.4f;1" % (acc_len / total_len, acc_len / total_len)
        )
        anim.setAttribute("dur", "%.4fs" % total_len)
        anim.setAttribute("begin", "0s;%s" % aid)
        anim.setAttribute("fill", "freeze")
        # Repeat on click
        anim.setAttribute("begin", "0s;%s.click" % aid)
        anim.setAttribute("repeatCount", "indefinite")
        number.appendChild(anim)

        acc_len = acc_len + wait

        # We set the dash length to the complete length of the path
        path.setAttribute("stroke-dasharray", "%.4f" % l)
        # Then we offset it by the same length, causing the path to disappear
        # We can then animate the drawing by varying this offset
        path.setAttribute("stroke-dashoffset", "%.4f" % l)

        dt = draw_time + l * length_factor

        # Hide the stroke before it's time to draw it. I do this because
        # sometimes on some browsers a tiny bit of the stroke is visible even
        # with the offset set to the right length (も does it at the top of the
        # vertical stroke for instance on android and chromium)
        anim = dom.createElement("animate")
        anim.setAttribute("attributeName", "opacity")
        anim.setAttribute("values", "0;0;1;1")
        anim.setAttribute(
            "keyTimes",
            "0;%.4f;%.4f;1" % (acc_len / total_len, acc_len / total_len),
        )
        anim.setAttribute("dur", "%.4fs" % total_len)
        anim.setAttribute("begin", "0s;%s" % aid)
        anim.setAttribute("fill", "freeze")
        # Repeat on click
        anim.setAttribute("begin", "0s;%s.click" % aid)
        anim.setAttribute("repeatCount", "indefinite")
        path.appendChild(anim)

        # Finally the main stroke animation
        anim = dom.createElement("animate")
        anim.setAttribute("attributeName", "stroke-dashoffset")
        anim.setAttribute("values", "%.4f;%.4f;0;0" % (l, l))
        anim.setAttribute(
            "keyTimes",
            "0;%.4f;%.4f;1" % (acc_len / total_len, (acc_len + dt) / total_len),
        )
        anim.setAttribute("dur", "%.4fs" % total_len)
        anim.setAttribute("begin", "0s;%s" % aid)
        anim.setAttribute("fill", "freeze")
        # Repeat on click
        anim.setAttribute("begin", "0s;%s.click" % aid)
        anim.setAttribute("repeatCount", "indefinite")
        path.appendChild(anim)

        acc_len = acc_len + dt


def recolor_svg_file(svg_file, out_svg_file):
    dom = minidom.parse(svg_file)
    paths, attributes = svg2paths(svg_file)

    # We bias upwards to avoid rounding errors that could cause visual glitches
    path_lengths = [p.length() + 0.01 for p in paths]

    recolor_svg(dom, path_lengths)

    with open(out_svg_file, "w") as f:
        dom.writexml(f)


if __name__ == "__main__":
    # recolor_svg_file('kanji/{:05x}.svg'.format(ord('一')), '/tmp/test.svg')
    # recolor_svg_file('kanji/{:05x}.svg'.format(ord('警')), '/tmp/test.svg')
    # recolor_svg_file('kanji/{:05x}.svg'.format(ord('学')), '/tmp/test.svg')
    # recolor_svg_file('kanji/{:05x}.svg'.format(ord('白')), '/tmp/test.svg')
    # recolor_svg_file('kanji/{:05x}.svg'.format(ord('蛙')), '/tmp/test.svg')
    # exit(0)

    files = [
        f for f in listdir("kanji") if isfile(join("kanji", f)) and f.endswith(".svg")
    ]
    if not isdir("kanji_colored"):
        mkdir("kanji_colored")
    for c, f in enumerate(sorted(files)):
        print("%s/%s: %s" % (c, len(files), f), end="\r")
        recolor_svg_file(join("kanji", f), join("kanji_colored", f))
    print("Recolored %s files" % len(files))
