#!/usr/bin/env python3

import genanki
import jaconv
import os

model = genanki.Model(
    1223687698,
    "Kana drawing",
    fields=[
        {"name": "Hiragana"},
        {"name": "Katakana"},
        {"name": "HiraganaFuri"},
        {"name": "KatakanaFuri"},
        {"name": "Romaji"},
        {"name": "StrokesHiragana"},
        {"name": "StrokesKatakana"},
    ],
    templates=[
        {
            "name": "KatakanaToHiragana",
            "qfmt": """カタカナ → ひらがな<br>
                <div class='hidefuri' style="text-align: center;font-size: xxx-large;">{{furigana:KatakanaFuri}}</div><br>
                """,
            "afmt": """
                {{FrontSide}}<hr id="answer">
                <div class='hidefuri'style="text-align: center;font-size: xxx-large;">
                    {{furigana:HiraganaFuri}}
                </div><br>
                <div class="strokes" style="text-align: center;">{{StrokesHiragana}}</div><br>
                """,
        },
        {
            "name": "HiraganaToKatakana",
            "qfmt": """ひらがな → カタカナ<br>
                <div class='hidefuri' style="text-align: center;font-size: xxx-large;">{{furigana:HiraganaFuri}}</div><br>
                """,
            "afmt": """
                {{FrontSide}}<hr id="answer">
                <div class='hidefuri'style="text-align: center;font-size: xxx-large;">
                    {{furigana:KatakanaFuri}}
                </div><br>
                <div class="strokes" style="text-align: center;">{{StrokesKatakana}}</div><br>
                """,
        },
    ],
    css="""
        .hidefuri ruby rt { visibility: hidden; }
        .hidefuri ruby:active rt { visibility: visible; }
        .hidefuri ruby:hover rt { visibility: visible; }

        .strokes {
            width: 65%;
            max-width: 500px;
            margin-left: auto;
            margin-right: auto
        }
    """,
)


class KanaNote(genanki.Note):
    @property
    def guid(self):
        return genanki.guid_for(self.fields[0])


ALL_HIRAGANA = [
    "あ",
    "い",
    "う",
    "え",
    "お",
    "か",
    "き",
    "く",
    "け",
    "こ",
    "さ",
    "し",
    "す",
    "せ",
    "そ",
    "た",
    "ち",
    "つ",
    "て",
    "と",
    "な",
    "に",
    "ぬ",
    "ね",
    "の",
    "は",
    "ひ",
    "ふ",
    "へ",
    "ほ",
    "ま",
    "み",
    "む",
    "め",
    "も",
    "や",
    "ゆ",
    "よ",
    "ら",
    "り",
    "る",
    "れ",
    "ろ",
    "わ",
    "を",
    "ん",
]

ALL_KATAKANA = [jaconv.hira2kata(h) for h in ALL_HIRAGANA]
ALL_ROMAJI = [jaconv.kana2alphabet(h) for h in ALL_HIRAGANA]

if __name__ == "__main__":
    if not os.path.isdir("kanji_colored"):
        print("Use recolor.py to generate the kanji SVG files")
        exit(1)

    deck = genanki.Deck(1310028423, "Kana drawing practice")

    media_files = []

    # kata to hira
    for h, k, r in zip(ALL_HIRAGANA, ALL_KATAKANA, ALL_ROMAJI):
        svg_h = "{:05x}.svg".format(ord(h))
        strokes_h = '<img src="{}">'.format(svg_h)
        svg_k = "{:05x}.svg".format(ord(k))
        strokes_k = '<img src="{}">'.format(svg_k)

        note = KanaNote(
            model=model,
            fields=[
                h,
                k,
                h + "[" + r + "]",
                k + "[" + r + "]",
                r,
                strokes_h,
                strokes_k,
            ],
        )
        deck.add_note(note)

        media_files.append("kanji_colored/" + svg_h)
        media_files.append("kanji_colored/" + svg_k)

    package = genanki.Package(deck)
    package.media_files = media_files
    package.write_to_file("/tmp/kana.apkg")
