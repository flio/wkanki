#!/usr/bin/env python3

import json
import datetime
import jaconv

with open("wk-kanji.json") as f:
    print("Loading Kanji data from wk-kanji.json")
    all_kanji = json.load(f)

with open("keisei.json") as f:
    print("Loading keisei data from keisei.json")
    keisei = json.load(f)


with open("kanjidic.json") as f:
    print("Loading Kanjidic data from kanjidict.json")
    kanjidic = json.load(f)

date = datetime.date.today()

index = {
    "title": "WK kanji keisei",
    "format": 3,
    "revision": date.strftime("WK-kanji-info_%Y-%m-%d"),
    "author": "Lionel Flandrin",
    "url": "https://gitlab.com/flio/wkanki",
    "description": "WaniKani kanji info",
    "sequenced": False,
}

tag_bank = [
        [
            "wk_level",
            "misc",
            0,
            "WaniKani level",
            0,
        ],
        [
            "semantic_component",
            "misc",
            1,
            "Semantic component",
            0,
        ],
        [
            "phonetic_component",
            "misc",
            2,
            "Phonetic component",
            0,
        ],
        [
            "full_phonetic",
            "misc",
            3,
            "Perfect phonetic matches",
            0,
        ],
        [
            "partial_phonetic",
            "misc",
            4,
            "Partial phonetic matches",
            0,
        ],
        [
            "nomatch_phonetic",
            "misc",
            5,
            "Phonetic mismatches",
            0,
        ],
]

kanji_meta = []

for v in all_kanji:
    data = v["data"]
    kanji = data["characters"]
    level = data["level"]

    if kanji not in kanjidic:
        continue

    meanings = [ m["meaning"] for m in data["meanings"] if m["primary"] ]
    kun = " ".join([ m["reading"] for m in data["readings"] if m["type"] == "kunyomi" ])
    on = " ".join([ jaconv.hira2kata(m["reading"]) for m in data["readings"] if m["type"] == "onyomi" ])

    psd = keisei.get(kanji, {})

    stats = {
            "wk_level": str(level),
            }

    if psd["kyuujitai"]:
        stats["kyuujitai"] = psd["kyuujitai"]

    if psd["semantic"]:
        stats["semantic_component"] = psd["semantic"]

    if psd["phonetic"]:
        stats["phonetic_component"] = psd["phonetic"]

    phonetic_partial = list()
    phonetic_full = list()
    phonetic_nomatch = list()

    char_on = set(kanjidic[kanji]["on"])

    for p in psd["phonetic_series"]:
        if p == kanji:
            continue

        if p not in kanjidic:
            continue

        p_on_r = kanjidic[p]["on"]
        p_on = set(kanjidic[p]["on"])

        common = p_on & char_on

        if p_on == char_on:
            phonetic_full.append((p, p_on_r))
        elif common:
            phonetic_partial.append((p, [o for o in p_on_r if o in common]))
        else:
            phonetic_nomatch.append((p, []))

    if phonetic_full:
        stats["full_phonetic"] = "".join(sorted([ p[0] for p in phonetic_full]))

    if phonetic_partial:
        stats["partial_phonetic"] = "".join(sorted([ p[0] for p in phonetic_partial]))

    if phonetic_nomatch:
        stats["nomatch_phonetic"] = "".join(sorted([ p[0] for p in phonetic_nomatch]))

    entry = [
            kanji,
            on,
            kun,
            "",
            meanings,
            stats
        ]
    kanji_meta.append(entry)

with open("yomichan-kanji/index.json", "w") as f:
    json.dump(index, f, indent=2, ensure_ascii=False)

with open("yomichan-kanji/tag_bank_1.json", "w") as f:
    json.dump(tag_bank, f, indent=2, ensure_ascii=False)


with open("yomichan-kanji/kanji_bank_1.json", "w") as f:
    json.dump(kanji_meta, f, indent=2, ensure_ascii=False)
