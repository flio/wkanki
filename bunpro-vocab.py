#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
import json

decks = [
    (5, "Vocab", "https://bunpro.jp/decks/resqiy/Bunpro-N5-Vocab"),
    (4, "Vocab", "https://bunpro.jp/decks/lh0vxb/Bunpro-N4-Vocab"),
    (3, "Vocab", "https://bunpro.jp/decks/mvt76c/Bunpro-N3-Vocab"),
    (2, "Vocab", "https://bunpro.jp/decks/dxbsvk/Bunpro-N2-Vocab"),
    (1, "Vocab", "https://bunpro.jp/decks/qqovik/Bunpro-N1-Vocab"),
    (5, "Grammar", "https://bunpro.jp/decks/nn10ai/Bunpro-N5-Grammar"),
    (4, "Grammar", "https://bunpro.jp/decks/m7omkx/Bunpro-N4-Grammar"),
    (3, "Grammar", "https://bunpro.jp/decks/cgwh1b/Bunpro-N3-Grammar"),
    (2, "Grammar", "https://bunpro.jp/decks/a5gf35/Bunpro-N2-Grammar"),
    (1, "Grammar", "https://bunpro.jp/decks/lzqrdc/Bunpro-N1-Grammar"),
]

terms = []
terms_simple = []

for deck, ty, url in decks:
    for page in range(1, 100):
        p = requests.get(url, params={"page": page})
        if not p.ok:
            break
        print("Parsing {}".format(p.url))
        soup = BeautifulSoup(p.content, "html.parser")
        vocab = [
            p.text for p in soup.find_all("p", attrs={"class": "v-text_large--400"})
        ]
        for v in vocab:
            terms.append(
                [v, "freq", {"value": deck, "displayValue": "{}-N{}".format(ty, deck)}]
            )
            terms_simple.append(
                {
                    "term": v,
                    "deck_type": ty,
                    "deck_name": "N{}".format(deck),
                }
            )

with open("bunpro/term_meta_bank_1.json", "w") as f:
    json.dump(terms, f, indent=2, ensure_ascii=False)

with open("bunpro/deck_index.json", "w") as f:
    json.dump(terms_simple, f, indent=2, ensure_ascii=False)
