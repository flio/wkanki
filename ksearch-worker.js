"use strict";

let dict = {};
let komponents = {};
let radicals = {};
let matches = [];
let matches_dirty = false;
let search_chunk_len = 1000;
let search_chunk_start = 0;
let search_tout = undefined;
let search_data = undefined;
let target_components = [];
let compiled_pattern = undefined;
let raw_target_components = [];
let secondary_target_components = [];
let matched_words = new Set()
let exact_compiled_pattern = undefined;

self.onmessage  = function (event) {
    let data = event.data;
    console.log("worker rx", data);
    switch (data.action) {
    case "search":
        matches = [];
        matched_words = new Set();
        matches_dirty = true;
        search_chunk_start = 0;
        search_data = data;
        if (search_tout) {
            clearTimeout(search_tout);
        }
        start_search();
        break;
    default:
        console.error("Unknown action", data);
    }
}

function start_search() {
    let pattern = search_data.pattern;
    target_components = [];
    // Same as above but only contains the components explicitly provided.
    //
    // So if the users specifies 「姫」:
    //  - target_components = [{ 姫, 女, 皿 }]
    //  - raw_target_components = [{ 姫 }]
    raw_target_components = [];
    secondary_target_components = [];
    exact_compiled_pattern = undefined;

    const component_block_re = /「(.*?)」/g;

    let match;
    while ((match = component_block_re.exec(pattern))) {
        let components = match[1];
        let cs = new Set();
        let raw_cs = new Set();

        for (let i = 0; i < components.length; i++) {
            let c = components[i];
            cs.add(c);
            raw_cs.add(c);

            // If the component is itself a kanji that can be broken down, add
            // the individual components too
            let komp = komponents[c] || new Set();

            komp.forEach(function (k) {
                cs.add(k);
            })
        }

        target_components.push(cs);
        raw_target_components.push(raw_cs);
        secondary_target_components.push(secondary_components(raw_cs));
    }

    pattern = pattern.replaceAll('？', '.')
        .replaceAll('＊', '.+?')
        .replaceAll(component_block_re, '([\u4E00-\u9FAF\u3400-\u4DBF])');

    target_components = target_components;
    raw_target_components = raw_target_components;

    if (target_components.length || has_kanji(pattern) || has_kana(pattern)) {
        pattern = pattern.replace(/\s/g, "");

        let dec = deconjugate(pattern);

        if (dec !== pattern) {
            exact_compiled_pattern = new RegExp(pattern);
            pattern = dec;
        }
        search_tout = setTimeout(do_search_chunk, 0);
    } else {
        search_tout = setTimeout(do_english_search_chunk, 0);
    }

    compiled_pattern = new RegExp(pattern);
}

function do_search_chunk() {
    dict.slice(search_chunk_start, search_chunk_start + search_chunk_len).forEach(function (d, i) {
        for (w of d.kanji_forms) {
            let search_idx = search_chunk_start + i;

            if (matched_words.has(search_idx)) {
                return;
            }

            let found = compiled_pattern.exec(w);
            if (!found) {
                return undefined;
            }

            // How many characters weren't part of the match
            let extra_match = w.length - found[0].length;

            let captures = found.slice(1);

            let match = {
                word: w,
                score: 0,
                captures: [],
                match_offset: found.index,
                match_len: found[0].length,
            };

            captures.forEach(function (c, i) {
                let target = target_components[i];
                let raw_target = raw_target_components[i];
                let secondary_target = secondary_target_components[i];
                let komp = komponents[c];
                let score = 0;

                if (match.score < -20) {
                    // Not worth wasting cycles
                    return;
                }

                if (!target || !komp) {
                    return;
                }

                if (raw_target.has(c)) {
                    // If this is a component that the user has provided then
                    // it's probably not what we're looking for
                    match.score -= raw_target.size - 1;
                    return;
                }

                let intersection = target.intersection(komp);
                score = intersection.size;

                let secondary_intersection = secondary_target.intersection(komp);
                score += secondary_intersection.size * 0.9;

                if (score == 0) {
                    // Nothing in common, probably not what we're looking for
                    match.score -= 100;
                    return;
                }

                // Penalize any missing primary component, but compensate with the
                // extra intersection
                let missing = raw_target.difference(komp);
                score -= Math.max(missing.size - secondary_intersection.size, 0);

                // Slightly penalize extra components to put simpler kanji first
                let extra = komp.difference(target).difference(secondary_target);
                score -= extra.size / 10;

                match.score += score;
                match.captures.push({
                                    target: raw_target,
                                    candidate: c,
                                    intersection: intersection,
                                    secondary_intersection: secondary_intersection,
                                    missing: missing,
                                    extra: extra,
                });
            })

            match.freq = d.freq;

            if (match.score >= 0) {
                if (exact_compiled_pattern && !exact_compiled_pattern.exec(w)) {
                    // This is a 2ndary form
                    match.score -= 1;
                }

                // Penalize extra-long matches. We do this here because we still
                // want to include the matches even if they're super long
                match.score -= extra_match;

                // Slightly bump more common words
                match.score -= Math.log10(match.freq) / 3;

                match.dict = d;
                matches.push(match);
                matched_words.add(search_idx);
                matches_dirty = true;
            }
        });
    }

    search_chunk_start += search_chunk_len;

    send_search_matches();

    if (search_chunk_start < dict.length) {
        search_tout = setTimeout(do_search_chunk, 0);
    } else if (target_components.length == 0 && !has_kanji(search_data.pattern)) {
        search_chunk_start = 0;
        search_tout = setTimeout(do_kana_search_chunk, 0);
    } else {
        search_done();
    }
}

function do_kana_search_chunk() {
    dict.slice(search_chunk_start, search_chunk_start + search_chunk_len)
        .forEach(function (d, i) {
            let search_idx = search_chunk_start + i;
            // Iterate over the readings
            d.readings.forEach(function (w) {
                if (matched_words.has(search_idx)) {
                    return;
                }

                let found = compiled_pattern.exec(w);
                if (!found) {
                    return;
                }

                // How many characters weren't part of the match
                let extra_match = w.length - found[0].length;

                let match = {
                    word: d.word,
                    freq: d.freq,
                    score: 0,
                };

                if (match.score >= 0) {
                    if (exact_compiled_pattern && !exact_compiled_pattern.exec(w)) {
                        // This is a 2ndary form
                        match.score -= 1;
                    }
                    // Penalize extra-long matches. We do this here because we still
                    // want to include the matches even if they're super long
                    match.score -= extra_match;

                    match.dict = d;
                    matches.push(match);
                    matched_words.add(search_idx);
                    matches_dirty = true;
                }
            });
    });

    search_chunk_start += search_chunk_len;

    dict.slice(search_chunk_start, search_chunk_start + search_chunk_len)

    send_search_matches();

    if (search_chunk_start < dict.length) {
        search_tout = setTimeout(do_kana_search_chunk, 0);
    } else {
        search_done();
    }
}

function do_english_search_chunk() {
    dict.slice(search_chunk_start, search_chunk_start + search_chunk_len)
        .forEach(function (d, i) {
            let search_idx = search_chunk_start + i;
            // Iterate over the readings
            d.meanings.forEach(function (w) {
                if (matched_words.has(search_idx)) {
                    return;
                }

                let found = compiled_pattern.exec(w);
                if (!found) {
                    return;
                }

                // How many characters weren't part of the match
                let extra_match = w.length - found[0].length;

                let score = 5 - Math.log10(d.freq) - extra_match / 10;

                let match = {
                    word: d.word,
                    score: score,
                    freq: d.freq,
                    dict: d,
                };

                matches.push(match);
                matched_words.add(search_idx);
                matches_dirty = true;
            });
    });

    search_chunk_start += search_chunk_len;

    dict.slice(search_chunk_start, search_chunk_start + search_chunk_len)

    send_search_matches();

    if (search_chunk_start < dict.length) {
        search_tout = setTimeout(do_kana_search_chunk, 0);
    } else {
        search_done();
    }
}
function send_search_matches() {
    if (!matches_dirty) {
        return;
    }

    matches.sort(function (a, b) {
        // Highest score first
        if (a.score != b.score) {
            return b.score - a.score;
        }

        // Highest frequency first (lower is better)
        if (a.freq != b.freq) {
            return a.freq - b.freq;
        }

        // Lexical ordering
        return a.word.localeCompare(b.word);
    });

    let search_progress = Math.round((search_chunk_start * 100) / dict.length);

    self.postMessage({
        action: "search_results",
        pattern: search_data.pattern,
        token: search_data.token,
        // Limit max entries
        matches: matches.slice(0, 50),
        nmatches: matches.length,
        progress: search_progress,
    });

    matches_dirty = false;
}

function search_done() {
    self.postMessage({
        action: "search_done",
        pattern: search_data.pattern,
        token: search_data.token,
    });
}

async function init() {
    let response = await fetch("kradfile.json");
    if (!response.ok) {
        throw new Error("Can't load kradfile.json: " + response.statusText)
    }
    komponents = await response.json();

    Object.keys(komponents).forEach(function (k) {
        komponents[k] = new Set(komponents[k]);
    });

    response = await fetch("jmdict.json");
    if (!response.ok) {
        throw new Error("Can't load jmdict.json: " + response.statusText)
    }
    dict = await response.json();

    response = await fetch("radicals.json");
    if (!response.ok) {
        throw new Error("Can't load radicals.json: " + response.statusText)
    }
    radicals = await response.json();

    search_chunk_len = dict.length / 5;

    self.postMessage({
                     action: "worker_ready",
    })
}
init();

function has_kanji(str) {
    const kanjiRegex = /[\u4E00-\u9FFF\u3400-\u4DBF]/;
    return kanjiRegex.test(str);
}

function has_kana(str) {
    const kanaRegex = /[\u3040-\u309F\u30A0-\u30FF]/;
    return kanaRegex.test(str);
}

let extra_components = {
    '人': '⺅个𠆢入',
    '𠆢': '人入',
    '心': '⺖',
    '手': '⺘',
    '衣': '衤',
    '示': '⺭',
    '⺭': '衤',
    '衤': '⺭',
    '火': '⺣',
    '刀': '⺉力',
    '力': '刀',
    '土': '十士',
    '十': '土士',
    '士': '土十',
    '夕': '歹',
    '歹': '夕',
    '生': '龶',
    // For 錆 & friends
    'ミ': '彡',
    '彡': 'ミ',
};

function secondary_components(primary) {
    let s = new Set();

    for (const k of primary) {
        let extra = extra_components[k] || '';

        for (let i = 0; i < extra.length; i++) {
            s.add(extra[i]);
        }
    };

    return s;
}

/* Polyfill for set intersection */
if (!Set.prototype.intersection) {
    Set.prototype.intersection = function(otherSet) {
        const out = new Set();
        for (const elem of otherSet) {
            if (this.has(elem)) {
                out.add(elem);
            }
        }
        return out;
    };
}

/* Polyfill for set difference */
if (!Set.prototype.difference) {
    Set.prototype.difference = function(otherSet) {
        const out = new Set();
        for (const elem of this) {
            if (!otherSet.has(elem)) {
                out.add(elem);
            }
        }
        return out;
    };
}

/* Table to convert potentially conjugated forms to dictionary form. The more
 * specific entries must come first since we stop after the first match */
const deconjugation_table = [
    // Verb ます forms
    [/(い(ます|ません(でした)?|ました))$/, '($1|いる|う)$'],
    [/(き(ます|ません(でした)?|ました))$/, '($1|きる|く|くる)$'],
    [/(ぎ(ます|ません(でした)?|ました))$/, '($1|ぎる|ぐ)$'],
    [/(し(ます|ません(でした)?|ました))$/, '($1|しる|す|する)$'],
    [/(ち(ます|ません(でした)?|ました))$/, '($1|ちる|つ)$'],
    [/(に(ます|ません(でした)?|ました))$/, '($1|にる|ぬ)$'],
    [/(び(ます|ません(でした)?|ました))$/, '($1|びる|ぶ)$'],
    [/(み(ます|ません(でした)?|ました))$/, '($1|みる|む)$'],
    [/(り(ます|ません(でした)?|ました))$/, '($1|り?る)$'],
    [/(ます|ません(でした)?|ました)$/, '($1|る)$'],

    // Godan verb negative
    [/(わ(ない|ぬ))$/, '($1|う)$'],
    [/(か(ない|ぬ))$/, '($1|く)$'],
    [/(が(ない|ぬ))$/, '($1|ぐ)$'],
    [/(さ(ない|ぬ))$/, '($1|す)$'],
    [/(な(ない|ぬ))$/, '($1|ぬ)$'],
    [/(ば(ない|ぬ))$/, '($1|ぶ)$'],
    [/(ま(ない|ぬ))$/, '($1|む)$'],
    [/(ら(ない|ぬ))$/, '($1|る)$'],
    // する special case
    [/(し(ない|ぬ))$/, '($1|する|しる)$'],
    // 来る special case
    [/(こ(ない|ぬ))$/, '($1|くる|こる)$'],

    // Adjectives た・て + negative
    [/(くない|かった|くなかった)$/, '($1|い)$'],
    [/(くて|くなくて)$/, '($1|い)$'],

    // Verb た・て forms
    // 来る special case
    [/(きって(い?る)?)$/, '($1|くる|きる|きう|きつ)$'],
    // 行く special case
    [/(いって(い?る)?)$/, '($1|いく|いる|いう|いつ)$'],
    [/(って(い?る)?)$/, '($1|る|う|つ)$'],
    [/(った)$/, '($1|る|う|つ)$'],
    [/(いて(い?る)?)$/, '($1|く|いる)$'],
    [/(いた)$/, '($1|く|いる)$'],
    [/(いで(い?る)?)$/, '($1|ぐ)$'],
    [/(いだ)$/, '($1|ぐ)$'],
    [/(んで(い?る)?)$/, '($1|む|ぶ|ぬ)$'],
    [/(んだ)$/, '($1|む|ぶ|ぬ)$'],
    [/(して(い?る)?)$/, '($1|する?|しる)$'],
    [/(した)$/, '($1|する?|しる)$'],
    [/(て(い?る)?)$/, '($1|る)$'],
    [/(た)$/, '($1|る)$'],

    // Ichidan verb negative (needs to come last not to match against godan and
    // adjectives)
    [/(ない|ぬ)$/, '($1|る)$'],

    // Imperative/命令形
    [/(え)$/, '($1|う)'],
    [/(け)$/, '($1|く)'],
    [/(げ)$/, '($1|ぐ)'],
    [/(せ)$/, '($1|す)'],
    [/(て)$/, '($1|つ)'],
    [/(ね)$/, '($1|ぬ)'],
    [/(べ)$/, '($1|ぶ)'],
    [/(め)$/, '($1|む)'],
    [/(れ)$/, '($1|る)'],
    // する special case
    [/(しろ)$/, '($1|する)'],
    // 来る special case
    [/(こい)$/, '($1|くる)'],
    // Ichidan
    [/(ろ)$/, '($1|る)'],

    // Causative/Causative-passive
    [/(こさせ(られ)?る)$/, '($1|こする?|くる)'],
    [/(わせ(られ)?る)$/, '($1|う)'],
    [/(かせ(られ)?る)$/, '($1|く)'],
    [/(がせ(られ)?る)$/, '($1|ぐ)'],
    [/(させ(られ)?る)$/, '($1|する?|る)'],
    [/(たせ(られ)?る)$/, '($1|つ)'],
    [/(なせ(られ)?る)$/, '($1|ぬ)'],
    [/(ばせ(られ)?る)$/, '($1|ぶ)'],
    [/(ませ(られ)?る)$/, '($1|む)'],
    [/(らせ(られ)?る)$/, '($1|る)'],

    // Passive
    // 来る special case
    [/(こられる)$/, '($1|こる|くる)'],
    [/(われる)$/, '($1|う)'],
    [/(かれる)$/, '($1|く)'],
    [/(がれる)$/, '($1|ぐ)'],
    [/(される)$/, '($1|する?)'],
    [/(たれる)$/, '($1|つ)'],
    [/(なれる)$/, '($1|ぬ)'],
    [/(ばれる)$/, '($1|ぶ)'],
    [/(まれる)$/, '($1|む)'],
    [/(られる)$/, '($1|る)'],

    // Potential
    [/(える)$/, '($1|う)'],
    [/(ける)$/, '($1|く)'],
    [/(げる)$/, '($1|ぐ)'],
    [/(せる)$/, '($1|す)'],
    [/(てる)$/, '($1|つ)'],
    [/(ねる)$/, '($1|ぬ)'],
    [/(べる)$/, '($1|ぶ)'],
    [/(める)$/, '($1|む)'],
    [/(れる)$/, '($1|る)'],

    // Volitional
    // する special case
    [/(しよう)$/, '($1|しる|する)'],
    // 来る special case
    [/(こよう)$/, '($1|こる|くる)'],
    [/(おう)$/, '($1|う)'],
    [/(こう)$/, '($1|く)'],
    [/(ごう)$/, '($1|ぐ)'],
    [/(そう)$/, '($1|す)'],
    [/(とう)$/, '($1|つ)'],
    [/(のう)$/, '($1|ぬ)'],
    [/(ぼう)$/, '($1|ぶ)'],
    [/(もう)$/, '($1|む)'],
    [/(ろう)$/, '($1|る)'],
    [/(よう)$/, '($1|る)'],
]

function deconjugate(w) {
    if (!has_kana(w.slice(-1))) {
        // If we don't end on a kana we can't have a conjugation
        return w;
    }

    for (let [re, conv] of deconjugation_table) {
        let s = w.replace(re, conv);

        if (s !== w) {
            return s;
        }
    }
    return w;
}

function test_deconjugation() {
    let should_match = function(conjugated, raw) {
        let p = new RegExp(deconjugate(conjugated));

        if (!p.exec(raw)) {
            console.error("Can't match", conjugated, "against", raw, p);
        }

        if (!p.exec(conjugated)) {
            console.error("Can't match", conjugated, "against itself", p);
        }
    };

    let tests = [
        ["帰って", "帰る"],
        ["死にます", "死ぬ"],
        ["見ます", "見る"],
        ["みます", "みる"],
        ["帰ります", "帰る"],
        ["帰って", "帰る"],
        ["見た", "見る"],
        ["上手かった", "上手い"],
        ["上手くない", "上手い"],
        ["上手くなかった", "上手い"],
        ["上手くて", "上手い"],
        ["上手くなくて", "上手い"],
        ["見ない", "見る"],
        ["みない", "みる"],
        ["いわない", "いう"],
        ["紡がない", "紡ぐ"],
        ["愛します", "愛する"],
        ["愛しない", "愛する"],
        ["愛して", "愛する"],
        ["しない", "しる"],
        ["します", "しる"],
        ["見ませんでした", "見る"],
        ["接して", "接しる"],
        ["接した", "接しる"],
        ["接します", "接しる"],
        ["歌え", "歌う"],
        ["がんばれ", "がんばる"],
        ["死ね", "死ぬ"],
        ["食べろ", "食べる"],
        ["歌われる", "歌う"],
        ["歌える", "歌う"],
        ["見れる", "見る"],
        ["見られる", "見る"],
        ["見させる", "見る"],
        ["休ませられる", "休む"],
        ["頑張ろう", "頑張る"],
        ["見よう", "見る"],
        ["しよう", "する"],
        ["こよう", "くる"],
    ];

    for (let [c, r] of tests) {
        should_match(c, r);
    }
}

if (self.location.hostname === 'localhost' || self.location.hostname === '127.0.0.1') {
    console.log('Running tests');
    test_deconjugation();
}
