#!/usr/bin/env python3

import genanki
import requests
import jaconv
from time import sleep
import os
import json
import csv
import re
import datetime
from xml.dom import minidom
from functools import reduce

token = os.environ.get("WK_TOKEN")

def build_synonyms():
    synonyms = [
        # One
        ["一", "壱", "甲"],
        # Two
        ["二", "弐", "乙"],
        # Three
        ["三", "参", "丙"],
        # Four
        ["四", "丁"],
        # Ten
        ["十", "拾"],
        # Reality
        ["真", "実"],
        # Build
        ["建", "築", "工"],
        # Fast/swift/quick/speed/rapid
        ["速", "早", "迅", "颯", "駿", "疾"],
        # Round
        ["丸", "円"],
        # Ball
        ["玉", "球"],
        # Reason
        ["理", "由", "訳", "智", "故"],
        # Origin
        ["元", "本", "原", "源"],
        # End/complete
        ["終", "了", "済", "末", "揃", "完", "全"],
        # Noon
        ["午", "昼"],
        # Inside/interior
        ["中", "内", "央", "奥", "衷"],
        # West
        ["洋", "西"],
        # Correct
        ["正", "当"],
        # Forest
        ["林", "森"],
        # Place
        ["場", "所"],
        # Road
        ["路", "道", "途"],
        # Study/learn
        ["習", "学", "覚"],
        # Sunlight
        ["陽", "光"],
        # Hot/warm
        ["暑", "温", "暖", "熱"],
        # I/me
        ["僕", "俺", "私", "我", "吾"],
        # Skill/technique
        ["技", "能", "術", "伎", "芸"],
        # Return
        ["帰", "返", "戻", "還"],
        # Season
        ["季", "節", "旬"],
        # Choose/select
        ["択", "選", "決", "詮"],
        # Good
        ["良", "宜", "冝", "宐"],
        # Guess
        ["察", "当"],
        # Near
        ["傍", "近"],
        # Times/occurences
        ["回", "度"],
        # Before/last
        ["先", "去", "前"],
        # After/next
        ["来", "後", "次", "翌"],
        # Death/die
        ["死", "亡", "没", "逝"],
        # Bowl
        ["丼", "鉢", "器", "椀"],
        # Merchandise/goods (in reality 商 doesn't have this meaning on its
        # own, but WK teaches it as "merchandise" which can create
        # confusion).
        ["商", "品", "貨"],
        # Say/talk
        ["言", "話", "語", "談", "喋"],
        # Change
        ["変", "化", "遷"],
        # Separate/partition
        ["分", "別", "頒", "訣"],
        # Task
        ["用", "務"],
        # Part
        ["部", "分"],
        # Leg
        ["足", "脚"],
        # Arm
        ["手", "腕", "肘"],
        # Hearth/soil/land
        ["地", "土", "陸", "壌"],
        # Meet
        ["合", "会", "遭", "遇"],
        # Fix
        ["治", "直", "繕"],
        # Heal/cure/treat
        ["療", "治", "癒", "矯", "遇"],
        # Climb/ascend
        ["登", "上", "昇"],
        # Raise (in the literal sense, not 育)
        ["上", "挙", "揚"],
        # Listen/hear
        ["聞", "聴"],
        # Wish/hope
        ["希", "望"],
        # Know
        ["知", "識"],
        # Government
        ["庁", "府", "政", "官", "署"],
        # Admonish/scold/chastise
        ["警", "諭", "誠", "喝", "苛", "叱", "懲", "討"],
        # Idea/thought
        ["意", "念", "考", "思", "想", "慮"],
        # Fear/scary/cowardice
        ["恐", "怖", "怯", "畏", "臆", "惧", "懼", "慄"],
        # Halt/stop
        ["停", "止", "佇"],
        # Defeat
        ["敗", "負"],
        # Love
        ["恋", "愛"],
        # Detailed/fine
        ["詳", "細", "緻"],
        # Thin
        ["痩", "細"],
        # Bind/entangle
        ["結", "縛", "絡"],
        # Together
        ["緒", "共"],
        # Evening/Night
        ["夕", "夜", "晩", "宵", "昏"],
        # Morning
        ["旦", "朝"],
        # Negative/refuse/decline
        ["不", "非", "否", "拒", "排", "断", "勿"],
        # Side
        ["横", "側", "際", "辺"],
        # Connection/connected
        ["係", "関", "連"],
        # Order/arrangement
        ["整", "順", "秩"],
        # Equal
        ["均", "等", "同"],
        # River
        ["川", "河"],
        # Quit/leave
        ["出", "辞", "罷", "去"],
        # Sword/blade
        ["剣", "刀", "刃"],
        # Child/childish/immature
        ["子", "児", "童", "稚", "幼"],
        # Write
        ["記", "書", "文"],
        # Work/employ
        ["働", "職", "勤", "雇", "傭"],
        # Paragraph
        ["句", "項"],
        # Street/road/avenue
        ["丁", "道", "街", "通"],
        # Request/want
        ["請", "求", "嘱", "願", "欲", "要", "需", "頼"],
        # Kind/type/sort
        ["種", "類", "彙"],
        # Seat
        ["座", "席"],
        # Hit/strike/slap
        ["打", "叩", "撲", "拍"],
        # Humble/humility
        ["慎", "謹", "遜", "謙"],
        # Luck/fortune
        ["福", "運", "吉", "占"],
        # Continue
        ["歴", "続"],
        # Trust
        ["信", "頼"],
        # Simple/easy/ease/simply/merely
        ["単", "易", "康", "朴", "簡", "唯", "只", "但"],
        # Group
        ["党", "団", "組"],
        # Insect
        ["虫", "昆"],
        # Generation
        ["世", "代"],
        # Replace
        ["代", "替", "換", "貿"],
        # Compare
        ["較", "以", "比", "対", "喩"],
        # Mix
        ["混", "交", "錯"],
        # Condition
        ["状", "況", "態"],
        # Form/shape/appearance
        ["形", "容", "姿", "態", "采", "貌"],
        # Garden
        ["畑", "園", "庭"],
        # Field
        ["田", "野", "原", "畑"],
        # Swamp
        ["沼", "沢"],
        # Shoe
        ["靴", "履"],
        # Wife
        ["妻", "婦"],
        # Exist
        ["存", "在", "有", "居", "顕"],
        # Fly
        ["翔", "飛"],
        # Damage/wound
        ["痛", "害", "傷", "損", "創"],
        # Crime/criminal
        ["犯", "罪", "囚"],
        # Theory
        ["説", "論"],
        # Passage of time
        ["歴", "経"],
        # Stink/smell
        ["臭", "匂", "嗅"],
        # Shake
        ["震", "揺", "振", "慄"],
        # Abundant/fertile
        ["富", "豊", "裕", "沃"],
        # Wealth
        ["富", "宝", "財", "福", "裕"],
        # Machine
        ["機", "台", "械"],
        # Sell
        ["売", "販"],
        # Hole
        ["坑", "穴", "孔"],
        # Certain
        ["必", "確"],
        # Righteous(ness)
        ["正", "義", "潔", "貞"],
        # Investigate
        ["調", "査", "検", "糾", "訊"],
        # Inspect/examine
        ["閲", "改", "査", "検", "視", "診"],
        # Whole/all
        ["総", "全", "随", "庶", "汎"],
        # Matter
        ["事", "物", "件", "儀", "質"],
        # Hill/mound
        ["坂", "岡", "阜", "丘", "塚", "陵"],
        # Slope
        ["阪", "坂", "勾"],
        # Cut/split/rip/peel
        ["切", "断", "斬", "刈", "裂", "析", "破", "綻", "剥"],
        # Criticism
        ["評", "批"],
        # Duty
        ["任", "役", "係", "務"],
        # Plan
        ["計", "挙", "案", "策", "企"],
        # Give birth
        ["生", "産"],
        # Add/increase
        ["加", "増", "弥", "摂"],
        # Judge
        ["判", "審", "裁"],
        # Previous
        ["先", "昨", "予", "既"],
        # Region/territory
        ["域", "境", "領"],
        # Drawing
        ["図", "画", "絵"],
        # Monk
        ["坊", "僧"],
        # Toil/exertion
        ["努", "労", "勉"],
        # Value/cost
        ["価", "値", "貴", "費"],
        # Abandon/discard
        ["投", "諦", "捨", "廃", "棄"],
        # Unite
        ["併", "統"],
        # Dark(ness)
        ["濃", "暗", "冥", "闇", "蒙", "曖", "昧", "瞑", "昏"],
        # Gentle
        ["優", "妥", "柔", "徐", "淑"],
        # Demon
        ["魔", "鬼"],
        # Defend/protect
        ["守", "防", "護", "保", "衛"],
        # Law/rule
        ["法", "律", "則", "憲", "典", "掟"],
        # Lead (as in guide, not the metal)
        ["連", "導"],
        # House/home/residence
        ["内", "屋", "家", "宅", "舎", "軒", "荘", "邸"],
        # Carry out/perform
        ["演", "施", "行"],
        # Hide/conceal
        ["隠", "没", "匿", "秘", "潜"],
        # Obtain/acquire
        ["収", "得", "獲", "取"],
        # Standard
        ["規", "準"],
        # Measure
        ["計", "規", "測", "寸", "量", "升"],
        # Carry/transport
        ["運", "担", "搬", "輸", "携"],
        # Steal/rob
        ["盗", "窃", "奪", "賊"],
        # Difference/differ
        ["差", "違", "異"],
        # Military/army
        ["兵", "武", "軍"],
        # Squad
        ["隊", "班"],
        # Pipe
        ["管", "笛", "筒"],
        # Ceremony
        ["式", "儀"],
        # Congratulate/praise/merit
        ["祝", "賀", "吉", "瑞", "慶", "寿", "勲"],
        # View/see/look at
        ["観", "景", "覧", "見", "眺", "視", "臨"],
        # Face/facing
        ["臨", "向"],
        # Wheel
        ["輪", "環", "舞", "軌"],
        # Family name
        ["姓", "氏"],
        # Burn/blaze
        ["火", "焼", "燃", "焦", "炎"],
        # Amount
        ["量", "程", "額", "数"],
        # Feeling/emotion
        ["感", "情", "懐"],
        # Wash
        ["洗", "濯", "汰"],
        # Freeze
        ["凝", "凍", "氷"],
        # Cold
        ["寒", "冷", "凛"],
        # Scold
        ["苛", "叱", "喝"],
        # Transit/traffic
        ["渡", "渉", "通"],
        # Search
        ["捜", "索", "探"],
        # Fresh
        # 生 doesn't have "fresh" as kanji meaning as far as I can tell, but
        # it's used to write なま so I think it's confusing
        ["鮮", "生"],
        # Catch/capture/apprehend/arrest/seize/captive
        ["捕", "捉", "囚", "逮", "拘", "獲", "虜", "掴", "勾"],
        # Receive/obtain
        ["受", "享", "戴", "貰", "賜"],
        # Village/city
        ["里", "町", "村", "郷", "街", "市"],
        # Sudden
        ["突", "唐", "颯", "急", "頓", "勃"],
        # Shadow
        ["影", "陰"],
        # Violent/violence/fierce
        ["暴", "激", "烈", "猛"],
        # Instruct/instruction
        ["授", "訓"],
        # Indicate/indication
        ["指", "徴", "示"],
        # Suspicious/suspect
        ["疑", "怪"],
        # Candy
        ["飴", "菓"],
        # Bring up/foster
        ["育", "養", "培"],
        # Stab/pierce
        ["突", "刺", "徹", "貫"],
        # Touch
        ["接", "触"],
        # Make/create/manufacture
        ["作", "造", "創", "製"],
        # Renew/restore
        ["復", "改", "更"],
        # Again/also/furthermore
        ["再", "更", "又", "猶", "且", "尚"],
        # Still/yet
        ["未", "猶", "尚"],
        # Impede/obstruct/thwart/hinder/prevent
        ["妨", "阻", "障", "防", "堰", "塞"],
        # Urge
        ["促", "迫", "奨"],
        # Encourage
        ["奨", "励"],
        # Tie
        ["結", "締", "括", "縛", "繋", "錮"],
        # Extreme
        ["極", "最"],
        # Edge
        ["際", "端", "縁"],
        # Peace(ful)
        ["康", "和", "安", "平", "泰", "靖"],
        # Salary
        ["俸", "給"],
        # Sleep
        ["睡", "寝", "眠", "瞑"],
        # Guy
        ["郎", "奴"],
        # Stomach
        ["胃", "腹"],
        # Invite/seduce
        ["呼", "招", "誘", "請", "召", "唆"],
        # Somebody
        ["身", "者", "誰"],
        # Number
        ["号", "番", "算", "第", "数"],
        # Thick
        ["厚", "濃", "膨"],
        # Pure
        ["潔", "清", "純", "精"],
        # Fail/fault
        ["失", "欠"],
        # Profit/benefit
        ["利", "益", "得", "効"],
        # Shoe
        ["履", "靴"],
        # Young
        ["稚", "若"],
        # Excel/excellence
        ["優", "秀", "雄", "傑", "佳"],
        # Short
        ["低", "短"],
        # Strange
        ["妙", "奇", "変", "異", "珍"],
        # Beauty/beautiful
        ["美", "秀", "麗", "綺", "斐", "佳"],
        # Attack
        ["撃", "攻", "討", "襲", "伐", "殴"],
        # Interval/period
        ["間", "期"],
        # Hero
        ["勇", "雄"],
        # Male
        ["男", "雄"],
        # Dry
        ["乾", "干", "渇", "燥"],
        # Skin/leather
        ["皮", "革", "膚", "肌"],
        # Sect
        ["派", "宗"],
        # Drink/sip
        ["喫", "吸", "飲", "呑"],
        # Offer/propose/present
        ["献", "提", "供", "案", "牲", "呈", "奉"],
        # Evil
        ["悪", "弊", "禍"],
        # Shop/store
        ["屋", "店", "舗"],
        # Cape
        ["岬", "埼", "崎"],
        # Marriage/Marry
        ["婚", "姻"],
        # Buy
        ["購", "買"],
        # Surpass/go beyond
        ["過", "越", "超"],
        # Wet/soak
        ["湿", "濡", "浸", "潤", "漬", "淹"],
        # Photograph/copy
        ["撮", "写", "謄"],
        # Samurai
        ["侍", "士"],
        # Edition/publish
        ["版", "刊", "載", "発", "著", "掲"],
        # Mark/remains/footprint/ticket
        ["符", "印", "跡", "標", "痕", "踪", "券"],
        # Flower/blossom
        ["花", "華", "咲"],
        # Help/rescue/assist
        ["助", "援", "救", "佐", "輔", "扶"],
        # Clothes/attire
        ["衣", "服", "装"],
        # Exceptional/special
        ["殊", "豪", "特"],
        # Luxury
        ["贅", "豪", "奢"],
        # Luxuriant
        ["茂", "繁", "滋"],
        # Warehouse/storehouse
        ["庫", "倉", "蔵"],
        # Model/imitation
        ["型", "鑑", "範", "擬", "模", "傲", "塑"],
        # Example/metaphor
        ["範", "例", "喩"],
        # Rank
        ["位", "級", "列"],
        # Row
        ["列", "並"],
        # Achievement/accomplishment
        ["功", "績", "遂", "達"],
        # Double/multiply
        ["倍", "複", "殖"],
        # Crowded
        ["混", "込"],
        # Gather/collect
        ["採", "拾", "集", "溜"],
        # Eye
        ["眼", "目"],
        # Secret
        ["密", "秘", "訣"],
        # Travel
        ["往", "旅"],
        # Ordinary
        ["普", "常", "但", "凡", "庸"],
        # Servant
        ["供", "臣", "隷", "僕", "奴"],
        # Back
        ["裏", "背", "後"],
        # Stature/height
        ["背", "丈", "脊"],
        # Hang
        ["掛", "垂", "懸", "吊"],
        # Long ago
        ["昔", "久", "既", "遥"],
        # Announce
        ["告", "宣"],
        # Steam
        ["汽", "蒸"],
        # Extend/prolong
        ["延", "拡", "伸"],
        # Consent/agree
        ["諾", "肯", "承", "賛", "頷"],
        # Shoot/fire (weapon)
        ["射", "撃", "放", "猟"],
        # Gun/arms
        ["銃", "武", "矛"],
        # Wave
        ["振", "揮"],
        # Recommend
        ["薦", "勧", "奨"],
        # Worship
        ["崇", "拝", "祀", "祭"],
        # Descend/fall
        ["降", "下", "落", "墜", "陥"],
        # Happiness/joy/rejoice
        [
            "喜",
            "賀",
            "歓",
            "吉",
            "悦",
            "享",
            "嬉",
            "慶",
            "祉",
            "幸",
            "祥",
            "愉",
        ],
        # Pleasant
        ["愉", "快", "楽"],
        # Branch
        ["支", "枝"],
        # Mistake
        ["違", "非", "誤"],
        # Confused/confusion/lost/misguided
        ["紛", "混", "狂", "慌", "錯", "惑", "迷"],
        # Astray
        ["迷", "紛"],
        # Vertical/dangle
        ["垂", "縦"],
        # Virtue/virtuous
        ["徳", "仁", "善"],
        # Self/Oneself
        ["自", "己"],
        # Carve
        ["刻", "彫", "刊"],
        # Boisterous/noisy
        ["喧", "噌", "騒", "嘩"],
        # Parent/relative
        ["親", "戚"],
        # Container/bowl/pot/plate/bottle
        ["鉢", "陶", "鍋", "器", "皿", "椀", "壺", "瓶"],
        # Slow
        ["遅", "鈍", "徐"],
        # Mutual
        ["相", "互"],
        # Reduce/decrease
        ["減", "縮", "耗"],
        # Former
        ["元", "先", "旧", "曽"],
        # Leader/commander/military officer
        ["長", "将", "覇", "主", "帥", "尉"],
        # Leave behind/set aside
        ["残", "置", "遺", "措"],
        # Stretch/extend
        ["張", "延", "伸", "拡"],
        # Reach/attain/arrive
        ["達", "届", "及", "至", "到", "着", "逐", "詣"],
        # Sea/ocean
        ["海", "洋", "沖"],
        # Rope
        ["縄", "索", "綱", "維", "紐"],
        # Port/harbour
        ["港", "津"],
        # Deed
        ["為", "伎"],
        # Pluck/extract
        ["抽", "摘", "抜"],
        # Nerve
        ["肝", "胆"],
        # Punish
        ["罰", "刑", "懲", "伐"],
        # Resist/endure
        ["抵", "耐", "抗", "堪", "凌", "忍"],
        # Fragrance/fragrant
        ["臭", "匂", "香", "芳"],
        # Bright
        ["明", "昭", "朗", "昌"],
        # Lapis Lazuli
        ["璃", "瑠"],
        # Inlet/bay
        ["江", "湾", "浦"],
        # Bribe
        ["賂", "賄"],
        # Join
        ["併", "合"],
        # Pregnant
        ["妊", "娠"],
        # Sugar/sugary
        ["糖", "甘"],
        # Send
        ["送", "還", "遣", "輸", "逓"],
        # Accompany/escort
        ["伴", "陪", "従", "送", "遵", "侶"],
        # Prosper/prosperity/flourish
        ["栄", "隆", "昌", "繁", "賑", "旺"],
        # Bury
        ["埋", "葬"],
        # Repress/hold back
        ["控", "抑"],
        # Jump/leap/hop
        ["跳", "躍", "飛", "踊"],
        # Bitter/acid
        ["苦", "酸", "渋", "酢"],
        # Start/first
        ["初", "始"],
        # Battle/fight/war
        ["戦", "争", "闘"],
        # Favor
        ["恩", "恵"],
        # Honor/honorable
        ["御", "誉"],
        # Shine/shining/glossy
        ["昭", "照", "輝", "艶", "眩"],
        # Medicine
        ["剤", "薬", "医"],
        # Reside/dwell
        ["住", "駐", "居", "棲"],
        # Sing/chant
        ["歌", "唱", "謡", "唄"],
        # Scream/yell/shout
        ["叫", "喚"],
        # Sharp/sharpen
        ["切", "研", "削", "鋭", "尖"],
        # Account/narrate
        ["紀", "記", "叙"],
        # Do
        ["仕", "致"],
        # Opposite/contrary
        ["逆", "対", "却"],
        # Lie/falsehood/deceit/cheat
        ["偽", "嘘", "詐", "欺", "謀", "騙"],
        # Withdraw
        ["控", "罷", "撤", "退"],
        # Praise
        ["賞", "賛", "称", "誉", "褒", "嘉"],
        # Fry
        ["揚", "炒"],
        # Sake/alcohol
        ["酎", "酒"],
        # Base/foundation/origin
        ["基", "礎", "本", "拠", "卑", "塁"],
        # Exclude/reject
        ["撤", "排", "除", "斥"],
        # Faithfulness/fidelity
        ["忠", "誠", "信"],
        # Wood/tree/timber
        ["樹", "木", "材"],
        # Supply
        ["補", "納", "収"],
        # Hard/solid
        ["緊", "確", "固", "堅", "硬", "牢"],
        # Temporary/temporarily
        ["仮", "暫"],
        # Every/each
        ["毎", "各"],
        # Inquire/ask
        ["伺", "尋", "糾", "問", "訊", "乞"],
        # Pattern
        ["範", "柄", "斐"],
        # Inn
        ["宿", "舎", "亭", "荘"],
        # Expose
        ["露", "披"],
        # Sink
        ["没", "沈"],
        # Rub/chafe/stroke/scratch
        ["摩", "擦", "抹", "撫", "爬"],
        # Erase/extinguish
        ["消", "抹"],
        # Seal
        ["封", "印", "璽"],
        # Destroy/break/crush
        ["壊", "破", "滅", "潰", "毀", "挫"],
        # Point/spot
        ["点", "旨", "斑"],
        # Simultaneous/concurrently
        ["斉", "兼"],
        # Wing
        ["翼", "羽"],
        # Interest
        ["興", "趣"],
        # Approach/draw close
        ["向", "寄", "挨", "拶"],
        # Approximate/about
        ["約", "頃", "概"],
        # Spirit/soul
        ["霊", "魂", "気", "心", "精", "神"],
        # Boat/ship
        ["船", "舟", "艇", "舶", "隻", "艦"],
        # Polish/train
        ["磨", "研", "摩", "練", "錬"],
        # Bell
        ["鈴", "鐘"],
        # Peak/summit
        ["頂", "峰", "岳", "峠"],
        # Desk/table
        ["卓", "机"],
        # Undress/naked
        ["脱", "裸"],
        # Miss/daughter
        ["嬢", "娘"],
        # To be
        ["居", "也"],
        # Fence
        ["垣", "塀", "壁", "柵"],
        # Clear/clarify
        ["澄", "晴", "鮮", "朗", "瞭", "亮", "昌", "彰", "冴"],
        # Bag
        ["袋", "俵"],
        # Hunt
        ["狩", "猟"],
        # Shelf
        ["架", "棚"],
        # Frame
        ["枠", "桟", "架"],
        # Emperor
        ["帝", "皇"],
        # Cool
        ["冷", "涼"],
        # Quiet
        ["寧", "穏", "静", "粛", "寂"],
        # Alone/lonely
        ["寂", "独", "孤"],
        # Leisure
        ["暇", "悠", "閑"],
        # Habit/accustomed
        ["慣", "癖"],
        # Pillar/column
        ["棟", "柱", "欄", "桁", "列"],
        # Valuable
        ["尊", "貴"],
        # Tray
        ["盆", "盤", "膳"],
        # Hat
        ["笠", "帽"],
        # Hate/dislike
        ["嫌", "憎"],
        # Rest
        ["憩", "休", "暇"],
        # Hanging scroll/width
        ["巾", "幅"],
        # Mourn/grieve/obituary
        ["喪", "悼", "弔", "忌", "悲", "嘆", "戚", "訃"],
        # Manipulate
        ["操", "御", "掌"],
        # Cook/boil/roast
        ["炊", "煮", "焼", "煎", "炒", "沸", "湧"],
        # Counterfeit/imitate/ressemble
        ["偽", "似", "擬", "肖", "倣"],
        # Age/years old
        ["歳", "齢", "才"],
        # Cultivate/cultivation/plantation
        ["拓", "培", "耕", "栽", "墾"],
        # Paint
        ["塗", "彩", "抹", "描", "染"],
        # Regret/resent/grudge
        ["憾", "悔", "恨", "悲", "憤", "怨", "仇"],
        # Grief/sad/melancholy/lament
        ["哀", "憂", "悼", "戚", "嘆", "悲", "鬱", "慨"],
        # Melt/dissolve
        ["融", "溶", "冶"],
        # Starve
        ["飢", "餓"],
        # Princess
        ["媛", "妃", "姫"],
        # Shame
        ["辱", "恥", "醜", "羞"],
        # Fishing
        ["漁", "釣"],
        # Crystal
        ["瑛", "晶"],
        # Grain
        ["粒", "穀"],
        # Pledge/promise/oath
        ["契", "約", "盟", "誓"],
        # Generally/universal
        ["般", "概", "遍", "普", "総"],
        # Relax
        ["安", "緩", "憩", "寛"],
        # Understand
        ["悟", "諒", "頒"],
        # Conquest
        ["克", "征"],
        # Awake
        ["覚", "醒"],
        # Pale
        ["淡", "蒼"],
        # Blue
        ["蒼", "青", "紺"],
        # Injustice
        ["非", "邪"],
        # China/chinese
        ["漢", "唐", "華", "中"],
        # Register/record
        ["籍", "簿", "録", "載"],
        # Revise/revision/correct
        ["改", "訂", "矯", "閲", "直"],
        # Consult/consultation
        ["議", "諮", "談"],
        # Tomb/grave/tombstone
        ["碑", "陵", "墳", "墓"],
        # Bundle
        ["束", "把"],
        # Calm
        ["穏", "泰"],
        # Robust/sturdy
        ["剛", "壮"],
        # Fat/obese/inflation
        ["太", "肥", "脂", "肪", "油", "膨", "騰"],
        # Explanation
        ["説", "釈"],
        # Corner
        ["角", "隅", "阿"],
        # County/district
        ["州", "郡", "区"],
        # Enclosure
        ["囲", "郭", "藩"],
        # Clan/clique
        ["閥", "氏", "藩", "派"],
        # Chastity
        ["操", "貞"],
        # Wise/clever/wisdom
        ["賢", "聡", "旨", "敏", "智"],
        # Embrace
        ["抱", "擁"],
        # Leader/leadership
        ["長", "覇", "将"],
        # Compensation/consolation/reparation/repay
        ["償", "賠", "慰", "酬"],
        # Exhibit/exposition
        ["呈", "陳", "博"],
        # Necessary/necessity/need
        ["須", "要", "需"],
        # Unnecessary/superfluous
        ["冗", "妄"],
        # Ridicule/humiliate
        ["蔑", "慢", "辱", "嘲", "弄"],
        # Lazy/laziness
        ["惰", "怠", "慢"],
        # Highness/majesty
        ["陛", "威"],
        # Threaten/threat
        ["威", "脅", "凄", "劫", "嚇"],
        # Purify/purification
        ["清", "浄", "斎"],
        # Rust
        ["錆", "丹"],
        # Horizontal
        ["緯", "横"],
        # Surplus
        ["余", "剰"],
        # Board/embark/ride
        ["乗", "搭"],
        # Scarlet/Red
        ["朱", "緋", "茜", "赤"],
        # Dawn
        ["暁", "旦", "曙"],
        # Enlighten
        ["悟", "啓"],
        # Disaster
        ["災", "惨"],
        # Stupid/foolish
        ["愚", "痴", "頑", "鈍"],
        # Neglect
        ["蔑", "疎", "怠"],
        # Proud/pride
        ["傲", "誇"],
        # Accumulate/amass/savings/deposit
        ["積", "累", "蓄", "貯", "預", "溜"],
        # Dragon
        ["竜", "龍"],
        # Seedling/sprout
        ["芽", "苗", "萌"],
        # Frugal
        ["倹", "惜"],
        # Guest
        ["客", "賓"],
        # Barbarian
        ["胡", "虜", "蛮"],
        # Rough
        ["荒", "粗"],
        # Of/の
        ["之", "乃"],
        # This
        ["之", "此"],
        # Distant
        ["遠", "懸", "悠", "遼", "遥"],
        # What
        ["哉", "那", "奈", "何"],
        # Bias
        ["傾", "偏"],
        # Insert
        ["挿", "入"],
        # Ordinary/common/normal
        ["庸", "常", "凡"],
        # Auspicious
        ["祥", "嘉"],
        # Praise
        ["嘉", "揚", "称", "褒", "誉", "賛", "賞"],
        # Practice
        ["練", "践"],
        # Revolve/rotate/spin/twirl
        ["糾", "繰", "紡", "旋", "転", "回", "捻"],
        # Spine
        ["呂", "椎"],
        # Respect/reverent/revered
        ["敬", "恭", "崇", "尊", "伺", "仰"],
        # Sew/weave/knit
        ["縫", "織", "編"],
        # Poor/destitute
        ["乏", "貧", "窮"],
        # Control
        ["制", "轄"],
        # Spread
        ["張", "敷", "羅", "氾"],
        # Crevice/ravine
        ["隙", "峡"],
        # Degenerate/decay/decline/wither
        ["堕", "腐", "朽", "衰", "枯", "萎"],
        # Give/gift
        ["与", "呉", "贈", "遣", "賜", "摯"],
        # Dare/daring
        ["冒", "敢"],
        # Mercy/pity/compassion
        ["慈", "恩", "憐", "哀"],
        # Play
        ["遊", "戯", "玩", "弄"],
        # Mud/muddy
        ["泥", "濁"],
        # Unclean/dirty
        ["濁", "醜", "汚"],
        # Run
        ["走", "奔", "駆"],
        # Go-between
        ["仲", "媒"],
        # Alternate
        ["隔", "迭"],
        # Divide
        ["割", "析", "剖"],
        # Anger
        ["憤", "怒"],
        # Greatness
        ["偉", "傑"],
        # Trample
        ["踏", "践"],
        # Sand
        ["沙", "砂"],
        # Poem
        ["詩", "詠", "賦", "韻"],
        # Draft
        ["稿", "案"],
        # Design
        ["綾", "柄", "企"],
        # Recite/recital
        ["詠", "唱", "吟"],
        # Scorn
        ["蔑", "侮", "軽"],
        # Set up/install/establish
        ["据", "構", "設"],
        # Intimate
        ["親", "睦"],
        # Guts
        ["腸", "胆"],
        # Wander/scatter
        ["浪", "散", "飛", "撒"],
        # Yearn
        ["慕", "憧", "懐", "恋", "憬"],
        # Very/Great/Exceedingly
        ["孔", "甚", "濫"],
        # Valley/ravine
        ["谷", "渓", "峡"],
        # Cheap
        ["安", "廉"],
        # Shore/coast/beach
        ["岸", "浜", "畔", "浦"],
        # Husband/groom
        ["夫", "婿"],
        # Gradually
        ["漸", "徐", "段"],
        # Reckless
        ["浪", "妄"],
        # Tax/tariff
        ["税", "租"],
        # Abduct/kidnap
        ["拉", "拐"],
        # Bend/warp
        ["曲", "折", "屈", "歪", "勾", "捻"],
        # Well
        ["湧", "井"],
        # Chess piece
        ["棋", "駒"],
        # Graceful
        ["麗", "雅", "淑"],
        # Article
        ["条", "款", "品"],
        # Prison/jail/cell
        ["牢", "獄", "虜", "檻"],
        # Drop
        ["滴", "雫"],
        # Dance
        ["舞", "躍", "踊"],
        # Close/shut
        ["塞", "閉"],
        # Anti-/-proof
        ["対", "耐"],
        # Armour
        ["甲", "鎧"],
        # Placement/put/place
        ["載", "置"],
        # Think/consider
        ["稽", "思", "考", "想", "憶", "慮"],
        # Sprout/stem/stalk
        ["芽", "茎", "梗"],
        # Stick/sticky
        ["粘", "貼"],
        # Counter for small things or pieces
        ["個", "箇"],
        # Counter for Sino-Japanese words
        ["箇", "ヶ", "ケ"],
        # Firewood
        ["柴", "薪"],
        # Overflow/spill
        ["溢", "零", "濫"],
        # Tower
        ["楼", "閣", "塔"],
        # Visit
        ["訪", "伺", "参"],
        # Squeeze/wring
        ["絞", "搾"],
        # Pile/pile up
        ["重", "盛", "積", "襲", "堆"],
        # Net
        ["帳", "網"],
        # Curtain
        ["幕", "帳"],
        # Imperial decree
        ["詔", "勅", "璽"],
        # Follow/pursue
        ["追", "逐", "辿"],
        # Heir/succeed
        ["嗣", "嫡", "継"],
        # Turn
        ["番", "逓", "順"],
        # Distribute
        ["頒", "配", "布"],
        # Attach
        ["付", "着", "添", "附"],
        # Furnace/kiln/cauldron
        ["炉", "窯", "釜"],
        # Throat
        ["喉", "咽"],
        # Small/few/little
        ["少", "小", "些", "僅"],
        # Swell/well up
        ["昜", "瘍", "湧", "腫"],
        # Bone
        ["骸", "骨"],
        # Capital
        ["京", "畿"],
        # Secluded/confinement
        ["錮", "幽"],
        # Temple
        ["刹", "寺", "院"],
        # Sefish
        ["恣", "我"],
        # Food
        ["食", "餌", "糧", "飯", "喰"],
        # Jealousy/envy/greed
        ["羨", "怨", "嫉", "欲", "妬", "貪"],
        # Tumor
        ["瘍", "腫"],
        # Sweep/brush/wipe/mop
        ["掃", "拭"],
        # Discussion/deliberation
        ["議", "談", "詮"],
        # Label/note/paper
        ["札", "票", "版", "箋", "紙"],
        # Advance
        ["進", "捗", "運", "駆"],
        # Fill/Fill up
        ["埋", "充", "填"],
        # Abuse/tyrannise
        ["弊", "虐", "罵"],
        # Capsize/cover
        ["覆", "蔽", "蓋", "塞", "被"],
        # Jaw/chin/cheeks
        ["顎", "頬"],
        # Spicy
        ["辛", "辣"],
        # Cruel
        ["辣", "酷", "惨", "暴"],
        # Foot of a mountain
        ["麓", "裾"],
        # Dog/puppy
        ["犬", "狗", "戌"],
        # Old man
        ["老", "尉", "翁", "爺"],
        # Pole/rod/cane/staff
        ["棒", "杖"],
        # Rare
        ["珍", "適", "希", "稀"],
        # Corpse
        ["骸", "屍"],
        # Mushroom/fungus
        ["菌", "茸", "蕈"],
        # Bark/howl/roar
        ["唸", "吠"],
        # Murmur/whisper
        ["呟", "囁"],
        # Stare/glare
        ["睨", "眺", "睥"],
        # Faint
        ["淡", "眩"],
        # Long time/eternity/long ages
        ["永", "久", "悠", "劫"],
        # Apologize/excuse
        ["謝", "免", "詫"],
        # Hollow/Cave (in)/Vacuum
        ["空", "洞", "凹", "窪", "陥", "穴", "窟"],
        # Helmet
        ["兜", "冑"],
        # Spear
        ["矛", "槍"],
        # Foe/enemy
        ["敵", "仇"],
        # Hesitate
        ["臆", "躊", "躇", "渋", "怯"],
        # Surprise
        ["驚", "愕"],
        # Rely/depend
        ["依", "憑"],
        # Chestnut
        ["栃", "栗"],
        # Nephew/niece
        ["姪", "甥"],
        # Soft/tender
        ["柔", "軟", "優"],
        # Attract/captivate/charm
        ["魅", "惹"],
        # Handle
        ["扱", "捌"],
        # Crawl
        ["爬", "這"],
        # Haze/Mist
        ["霞", "霧", "靄"],
        # Axe
        ["斤", "斧"],
    ]

    d = dict()

    for s in synonyms:
        seen = set()
        for kanji in s:
            if kanji in seen:
                raise Exception(f"Duplicate synonym for {kanji}")
            d.setdefault(kanji, set()).update([k for k in s if k != kanji])
            seen.add(kanji)

    return d


model = genanki.Model(
    1223687697,
    "Wanikani Kanji",
    fields=[
        {"name": "Kanji"},
        {"name": "WkMeaning"},
        {"name": "KanjidicMeaning"},
        {"name": "MergedMeaning"},
        {"name": "StrokeCount"},
        {"name": "Jlpt"},
        {"name": "Freq"},
        {"name": "On"},
        {"name": "Kun"},
        {"name": "Nanori"},
        {"name": "Level"},
        {"name": "Strokes"},
        {"name": "Radical"},
        {"name": "WkUrl"},
        {"name": "Synonyms"},
        {"name": "SimilarLooking"},
        {"name": "Keisei"},
        {"name": "Vocabulary"},
        {"name": "VocabularyFurigana"},
        {"name": "VocabularyKanaOnly"},
        {"name": "VocabularyCloze"},
        {"name": "VocabularyClozeFurigana"},
        {"name": "GenDate"},
    ],
    # Sort by level
    sort_field_index=0,
    templates=[
        {
            "name": "Draw",
            "qfmt": """WK Level {{Level}}<br>
                <div style="text-align: center;"><h3>{{MergedMeaning}}</h3></div><br>
                {{#StrokeCount}}
                Stroke count: {{StrokeCount}}<br>
                {{/StrokeCount}}
                {{#Synonyms}}
                Related kanji: {{Synonyms}}<br>
                {{/Synonyms}}
                {{#Radical}}
                <details>
                <summary>Radical</summary>
                <span class="radical-hint">{{Radical}}</class>
                </details>
                {{/Radical}}
                <br>{{VocabularyCloze}}<br>
                <br><div class="gendate">Card generated on {{GenDate}}</div>
                <div class="update-check">
                <a class="ext-link" href="https://ankiweb.net/shared/info/610839770">
                ↪ Check Ankiweb for updates
                </a></div>
                """,
            "afmt": """WK Level {{Level}}<br>
                <div style="text-align: center;"><h3>{{MergedMeaning}}</h3></div><br>
                <div style="text-align: center;font-size: xxx-large;">{{Kanji}}</div><br>
                <div class="strokes">{{Strokes}}</div><br>
                <details class="vocab-details">
                    <summary>Vocabulary</summary>
                    <br>{{furigana:VocabularyFurigana}}<br>
                </details><br>
                <table class="kanjidata">
                {{#On}}
                <tr><td>音読み</td><td>{{On}}</td></tr>
                {{/On}}
                {{#Kun}}
                <tr><td>訓読み</td><td>{{Kun}}</td></tr>
                {{/Kun}}
                {{#Nanori}}
                <tr><td>名乗り</td><td>{{Nanori}}</td></tr>
                {{/Nanori}}
                {{#Radical}}
                <tr><td>Radical</td><td>{{Radical}}</td></tr>
                {{/Radical}}
                {{#SimilarLooking}}
                <tr><td>Visually similar</td><td>{{SimilarLooking}}</td></tr>
                {{/SimilarLooking}}
                {{#Synonyms}}
                <tr><td>Related kanji</td><td>{{Synonyms}}</td></tr>
                {{/Synonyms}}
                {{#Keisei}}
                {{furigana:Keisei}}
                {{/Keisei}}
                {{#Freq}}
                <tr><td>Frequency ranking</td><td>{{Freq}}</td></tr>
                {{/Freq}}
                </table>
                <br>
                <a class="ext-link" href="{{WkUrl}}">↪ See {{Kanji}} on WaniKani</a><br>
                <br>
                <a class="ext-link" href="https://jisho.org/search/{{Kanji}}%20%23kanji">
                ↪ See {{Kanji}} on Jisho</a><br>
                <br>
                <a class="ext-link" href="https://en.wiktionary.org/wiki/{{Kanji}}#Japanese">
                ↪ See {{Kanji}} on the Wiktionary</a><br>
                </a>
                <br>
                <div class="gendate ext-link">Card generated on {{GenDate}}</div>
                <div class="update-check ext-link"><a href="https://ankiweb.net/shared/info/610839770">
                ↪ Check Ankiweb for updates
                </a></div>
                """,
        }
    ],
    css="""
        .card {
            text-align: center;
        }
        a {
            text-decoration: none;
        }
        a.ext-link {
            color: #408080;
        }
        .kanjidata {
            margin-left: auto;
            margin-right: auto;
        }
        .card td:nth-child(1) {
            padding-right: 1rem;
            text-align: left;
        }
        .strokes {
            width: 65%;
            max-width: 500px;
            margin-left: auto;
            margin-right: auto
        }
        .kcloze {
            opacity: 0.5;
            border: 1px solid #808080;
        }
        .vocab {
            margin-left: auto;
            margin-right: auto;
        }
        .gendate {
            color: grey;
            font-size: x-small;
        }
        .update-check a {
            color: grey;
            font-size: x-small;
        }
        .radical-hint {
            font-size: x-large;
        }
    """,
)

class KanjiNote(genanki.Note):
    @property
    def guid(self):
        return genanki.guid_for(self.fields[0])

def query(path, params=None):
    return requests.get(
        "https://api.wanikani.com/v2/" + path,
        params=params,
        headers={"Authorization": "Bearer " + token},
    )


def kanjis_for_level(level):
    resp = query("subjects", {"levels": level, "types": "kanji"})

    return resp.json()["data"]


def vocab_for_level(level):
    resp = query("subjects", {"levels": level, "types": "vocabulary"})

    return resp.json()["data"]


def jishoize_kanji(kanjis):
    if type(kanjis) is list:
        return [ f'<a href="https://jisho.org/search/%23kanji%20{k[0]}">{k}</a>'
                for k in kanjis ]
    else:
        return f'<a href="https://jisho.org/search/%23kanji%20{kanjis[0]}">{kanjis}</a>'

ALL_HIRAGANA = [
    "あ",
    "い",
    "う",
    "え",
    "お",
    "か",
    "き",
    "く",
    "け",
    "こ",
    "さ",
    "し",
    "す",
    "せ",
    "そ",
    "た",
    "ち",
    "つ",
    "っ"
    "て",
    "と",
    "な",
    "に",
    "ぬ",
    "ね",
    "の",
    "は",
    "ひ",
    "ふ",
    "へ",
    "ほ",
    "ま",
    "み",
    "む",
    "め",
    "も",
    "や",
    "ゆ",
    "よ",
    "ら",
    "り",
    "る",
    "れ",
    "ろ",
    "わ",
    "を",
    "ん",
    "が",
    "ぎ",
    "ぐ",
    "げ",
    "ご",
    "ざ",
    "じ",
    "ず",
    "ぜ",
    "ぞ",
    "だ",
    "ぢ",
    "づ",
    "で",
    "ど",
    "ば",
    "び",
    "ぶ",
    "べ",
    "ぼ",
    "ぱ",
    "ぴ",
    "ぷ",
    "ぺ",
    "ぽ",
    "ゃ",
    "ゅ",
    "ょ",
    "ー",
    "〜",
]

ALL_KATAKANA = [jaconv.hira2kata(h) for h in ALL_HIRAGANA]
ALL_KANA = set(ALL_HIRAGANA + ALL_KATAKANA)


def furiganize(word, reading):
    if not any(c in ALL_KANA for c in word):
        # All kanji
        return "%s[%s]" % (word, reading)

    if all(c in ALL_KANA for c in word):
        # All kana
        return word

    start = ""
    end = ""

    if word[0] in ["〜"]:
        start = word[0]
        word = word[1:]

    if word[-1] in ["〜"]:
        end = word[-1]
        word = word[:-1]

    wc = [c for c in word]
    rc = [c for c in reading]

    # Start by removing the kana at the start of the word
    for w, r in zip(word, reading):
        if jaconv.kata2hira(w) == jaconv.kata2hira(r):
            start = start + w
            wc.pop(0)
            rc.pop(0)
        else:
            break

    # Then remove kana at the end of the word
    for w, r in zip(reversed(word), reversed(reading)):
        if jaconv.kata2hira(w) == jaconv.kata2hira(r):
            end = w + end
            wc.pop(-1)
            rc.pop(-1)
        else:
            break

    if start:
        start = start + " "

    # If we don't have kana left in the center part, we're good
    if all(c not in ALL_KANA for c in wc):
        return "%s%s[%s]%s" % (start, "".join(wc), "".join(rc), end)

    # print("%s %s" % (''.join(wc), ''.join(rc)))

    mid = ""
    for c in wc:
        # print("c: %s [%s]"%( c, ''.join(rc)))
        if c in rc[1:]:
            if rc[1:-1].count(c) == 1:
                i = rc[1:].index(c) + 1
                furi = rc[:i]
                rc = rc[i + 1 :]
                if furi:
                    mid = mid + ("[%s]" % "".join(furi))
                mid = mid + c
            else:
                print("Can't furiganize %s[%s] %s" % (word, reading, c))
                return "%s[%s]" % (word, reading)
        elif c == rc[0]:
            mid = mid + c
            rc.pop(0)
        else:
            if mid and c not in ALL_KANA and mid[-1] in ALL_KANA:
                mid = mid + " "
            mid = mid + c

    if rc:
        mid = mid + ("[%s]" % "".join(rc))

    # print("%s%s%s" % (start, mid, end))
    return "%s%s%s" % (start, mid, end)


def test_furiganize():
    assert furiganize("大きい順", "おおきいじゅん") == "大[おお]きい 順[じゅん]"
    assert furiganize("包み紙", "つつみがみ") == "包[つつ]み 紙[がみ]"
    assert furiganize("言い訳", "いいわけ") == "言[い]い 訳[わけ]"
    assert furiganize("切り取る", "きりとる") == "切[き]り 取[と]る"
    assert furiganize("今", "いま") == "今[いま]"
    assert furiganize("元気", "げんき") == "元気[げんき]"
    assert furiganize("別に", "べつに") == "別[べつ]に"
    assert furiganize("お金", "おかね") == "お 金[かね]"
    assert furiganize("我が〜", "わが") == "我[わ]が〜"
    assert furiganize("消しゴム", "けしごむ") == "消[け]しゴム"
    assert furiganize("消しゴム", "けしごむ") == "消[け]しゴム"
    assert furiganize("元パートナー", "もとぱーとなー") == "元[もと]パートナー"


def format_vocab(vocab, kanji=True, furigana=False):
    r = '<table class="vocab">'
    [v["data"]["characters"] for v in vocab]
    for v in vocab:
        data = v["data"]
        reading = data["readings"][0]["reading"]
        if kanji:
            if furigana:
                c = furiganize(data["characters"], reading)
            else:
                c = data["characters"]
        else:
            c = reading

        meanings = [m["meaning"] for m in data["meanings"] if m["primary"]]

        r = r + ("<tr><td>%s</td><td>%s</td></tr>" % (c, ", ".join(meanings[:2])))
    r = r + "</table>"
    return r


def kanjidic_parse(kanjidic_file):
    # Get latest kanjidic data at
    #    http://www.edrdg.org/kanjidic/kanjidic2.xml.gz
    print("Loading Kanjidic data from %s" % kanjidic_file)
    dom = minidom.parse(kanjidic_file)

    kanji = dom.getElementsByTagName("character")

    r = {}

    print("Loading radical data from radicals-kangxi.json")
    with open("radicals-kangxi.json") as f:
        radicals = json.load(f)

    for k in kanji:
        character = k.getElementsByTagName("literal")[0]
        character = character.childNodes[0].data

        jlpt = k.getElementsByTagName("jlpt")
        if jlpt:
            jlpt = jlpt[0].childNodes[0].data
        else:
            jlpt = ""

        stroke_count = k.getElementsByTagName("stroke_count")[0]
        stroke_count = stroke_count.childNodes[0].data

        freq = k.getElementsByTagName("freq")
        if freq:
            freq = freq[0].childNodes[0].data
        else:
            freq = ""

        meanings = []
        for m in k.getElementsByTagName("meaning"):
            lang = m.getAttribute("m_lang")
            if lang in ["", "en"]:
                meaning = m.childNodes[0].data
                if meaning[0].islower():
                    meaning = meaning.title()
                    meaning = meaning.replace("'T", "'t")
                    meaning = meaning.replace("'S", "'s")
                meanings.append(meaning)

        on = []
        kun = []
        for m in k.getElementsByTagName("reading"):
            ty = m.getAttribute("r_type")
            if ty == "ja_on":
                reading = m.childNodes[0].data.title()
                on.append(reading)
            elif ty == "ja_kun":
                reading = m.childNodes[0].data.title()
                kun.append(reading)

        radical = ""
        for rad in k.getElementsByTagName("rad_value"):
            ty = rad.getAttribute("rad_type")
            if ty == "classical":
                n = int(rad.childNodes[0].data)
                radical = radicals[n - 1]["radical"]
                break

        r[character] = {
            "jlpt": jlpt,
            "stroke_count": int(stroke_count),
            "freq": freq,
            "meanings": meanings,
            "radical": radical,
            "on": on,
            "kun": kun,
        }

    return r


kanjidic = {}

try:
    with open("kanjidic.json") as f:
        print("Loading Kanjidic data from kanjidict.json")
        kanjidic = json.load(f)
except Exception as e:
    print("Failed to load cached Kanjidic data: %s" % e)

    kanjidic = kanjidic_parse("kanjidic2.xml")

    print("Dumping kanjidic.json")
    with open("kanjidic.json", "w") as f:
        json.dump(kanjidic, f, indent=2, ensure_ascii=False)

component_equiv = [
    ["糸", "糹", "纟"],
    ["肉", "⺼"],
    ["食", "飠"],
    ["竹", "𥫗"],
    ["水", "氵"],
    ["人", "亻"],
    ["手", "扌"],
    ["犬", "犭"],
    ["心", "忄", "㣺"],
    ["艸", "艹"],
    ["刀", "刂"],
    ["辵", "辶", "⻎", "⻌"],
    ["攴", "攵"],
    ["言", "訁"],
    ["示", "礻"],
    ["衣", "衤"],
    ["火", "灬"],
    ["足", "𧾷"],
    ["玉", "𤣩"],
    ["网", "罒"],
]

component_map = {}

for eq in component_equiv:
    for c in eq[1:]:
        component_map[c] = eq[0]

keisei = {}

with open("keisei.csv") as file:
    reader = csv.reader(file)
    # Skip header
    next(reader)

    def parse_csv_entry(e):
        e = e.strip()
        if not e or e == "NA" or e == "?":
            return ""

        if e in component_map:
            return component_map[e]

        return e

    by_phonetic_component = {}
    to_kyuujitai = {}

    # If your file has headers, you can skip the first row with next(reader)
    for row in reader:
        kanji = parse_csv_entry(row[1])

        if not kanji:
            continue

        phonetic = parse_csv_entry(row[6])
        kyuujitai = parse_csv_entry(row[4])
        v = {
            "kyuujitai": kyuujitai,
            "semantic": parse_csv_entry(row[5]),
            "phonetic": phonetic,
            "phonetic_series": set([kanji]),
            "secondary": row[0] == "Secondary",
        }

        if phonetic:
            p = by_phonetic_component.setdefault(phonetic, set([phonetic, kanji]))
        else:
            p = set()

        keisei[kanji] = v
        if kyuujitai:
            to_kyuujitai[kanji] = kyuujitai
            keisei[kyuujitai] = v.copy()
            keisei[kyuujitai]["secondary"] = True

    # Recursively merge phonetic-component-of-phonetic-component
    for k, v in keisei.items():
        if v["secondary"]:
            continue

        def find_phonetic_root(k):
            if k not in keisei:
                return k

            kv = keisei[k]
            if not kv["phonetic"]:
                return k

            if k == kv["phonetic"]:
                return k

            return find_phonetic_root(kv["phonetic"])

        pr = find_phonetic_root(k)

        # Normalize to kyuujitai if available
        if pr in to_kyuujitai:
            pr = to_kyuujitai[pr]

        v["phonetic_root"] = pr
        s = by_phonetic_component.setdefault(pr, set())
        s.add(k)
        if v["phonetic"]:
            s.add(v["phonetic"])
        v["phonetic_series"] = s

    with open("keisei.json", "w") as f:
        class SetEncoder(json.JSONEncoder):
            def default(self, obj):
                if isinstance(obj, set):
                    return sorted(list(obj))
                return json.JSONEncoder.default(self, obj)
        json.dump(keisei, f, indent=2, ensure_ascii=False, cls=SetEncoder)


def is_phono_semantic(char):
    if char not in keisei:
        return False

    return keisei[char]["phonetic"]


def get_keisei_data(char):
    if char not in kanjidic:
        return "", "", "", "", [], [], []

    if char not in keisei:
        print(f"No keisei entry for {char}")
        return "", "", "", "", [], [], []

    k = keisei[char]
    kyuujitai = k["kyuujitai"]
    phonetic = k["phonetic"]
    semantic = k["semantic"]

    phonetic_partial = list()
    phonetic_full = list()
    phonetic_nomatch = list()

    char_on = set(kanjidic[char]["on"])

    # If the character has no phonetic component, still see if it's a phonetic
    # component for other characters
    if not phonetic:
        phonetic = char

    # Try to find other kanji with the same onyomi and the same phonetic
    # component to display
    for p in k["phonetic_series"]:
        if p == char:
            continue

        if p not in kanjidic:
            continue

        p_on_r = kanjidic[p]["on"]
        p_on = set(kanjidic[p]["on"])

        common = p_on & char_on

        if p_on == char_on:
            phonetic_full.append((p, p_on_r))
        elif common:
            phonetic_partial.append((p, [o for o in p_on_r if o in common]))
        else:
            phonetic_nomatch.append((p, []))

    keisei_table = ""

    def format_matches(matches):
        s = list()
        for m, r in sorted(matches):
            if r:
                s.append(f" {jishoize_kanji(m + '[' + '/'.join(r) + ']')}")
            else:
                s.append(jishoize_kanji(m))

        return " ".join(s)

    if kyuujitai:
        keisei_table += f"<tr><td>旧自体</td><td>{kyuujitai}</td></tr>"
    if semantic:
        keisei_table += f"<tr><td>Semantic component</td><td>{jishoize_kanji(semantic)}</td></tr>"
    if phonetic != char:
        keisei_table += f"<tr><td>Phonetic component</td><td>{jishoize_kanji(phonetic)}</td></tr>"
    if phonetic_full:
        keisei_table += f"<tr><td>Perfect phonetic matches</td><td>{format_matches(phonetic_full)}</td></tr>"
    if phonetic_partial:
        keisei_table += f"<tr><td>Partial phonetic matches</td><td>{format_matches(phonetic_partial)}</td></tr>"
    if phonetic_nomatch:
        keisei_table += f"<tr><td>Phonetic mismatches</td><td>{format_matches(phonetic_nomatch)}</td></tr>"

    return (
        keisei_table,
        kyuujitai,
        phonetic,
        semantic,
        phonetic_full,
        phonetic_partial,
        phonetic_nomatch,
    )


def extract_kanji(text):
    # Regular expression for matching kanji characters
    kanji_pattern = r"[\u4E00-\u9FFF\u3400-\u4DBF\U00020000-\U0002A6DF]"

    # Find all kanji characters in the text
    kanji_characters = re.findall(kanji_pattern, text)

    return kanji_characters


lvlkanji = {}

max_lvl = 0

if __name__ == "__main__":
    test_furiganize()

    if not os.path.isdir("kanji_colored"):
        print("Use recolor.py to generate the kanji SVG files")
        exit(1)

    synonyms = build_synonyms()

    deck = genanki.Deck(1310028421, "Kanji drawing practice")

    media_files = []
    all_kanji = []
    all_vocab = []

    json_dump = {}

    try:
        with open("wk-kanji.json") as f:
            print("Loading Kanji data from wk-kanji.json")
            all_kanji = json.load(f)
    except Exception as e:
        print("Failed to load cached Kanji data: %s" % e)
        print("Fetching from api.wanikani")

        for level in range(1, 61):
            sleep(1)
            print("Fetching level %s" % level)
            kanjis = kanjis_for_level(level)
            all_kanji.extend(kanjis)

        print("Dumping wk-kanji.json")
        with open("wk-kanji.json", "w") as f:
            json.dump(all_kanji, f, indent=2, ensure_ascii=False)

    try:
        with open("wk-vocab.json") as f:
            print("Loading vocabulary data from wk-vocab.json")
            all_vocab = json.load(f)
    except Exception as e:
        print("Failed to load cached Vocab data: %s" % e)
        print("Fetching from api.wanikani")

        for level in range(1, 61):
            sleep(1)
            print("Fetching level %s" % level)
            vocab = vocab_for_level(level)
            all_vocab.extend(vocab)

        print("Dumping wk-vocab.json")
        with open("wk-vocab.json", "w") as f:
            json.dump(all_vocab, f, indent=2, ensure_ascii=False)

    for raw in all_kanji:
        kanji = raw["data"]
        level = kanji["level"]
        char = kanji["characters"]

        lvlkanji.setdefault(level, []).append(char)

        if level > max_lvl:
            max_lvl = level

    running_total = 0
    keisei_total = 0

    for lvl in range(1, max_lvl + 1):
        k = lvlkanji.get(lvl, set())
        count = len(k)
        running_total = running_total + count
        ps = sum(1 for c in k if is_phono_semantic(c))
        percent = round((ps * 100) / count)
        keisei_total += ps
        print(
            "{:>2}: {:>4} (+{}) (形声 {} - {}%): {}".format(
                lvl, running_total, count, ps, percent, "".join(k)
            )
        )
        # print(f"[{count}, {running_total}],")
    print(
        f"形声 total: {keisei_total} / {running_total} ({round(keisei_total * 100 / running_total)}%)"
    )

    with open("extra-kanji.json") as f:
        print("Loading extra kanji data from extra-kanji.json")
        extra = json.load(f)

        for k, d in extra.items():
            # Create "fake" WK data for this kanji
            kid = 100000 + ord(k)

            wkk = {
                "id": kid,
                "object": "kanji",
                "data": {
                    "level": 999,
                    "characters": k,
                    "meanings": [],
                    "readings": [],
                    "visually_similar_subject_ids": [],
                },
            }
            all_kanji.append(wkk)

    kanji_by_id = {k["id"]: k for k in all_kanji}
    kanji_by_char = {k["data"]["characters"]: k for k in all_kanji}

    def linkify_kanji(text):
        kanji_regex = r'[\u4e00-\u9faf]'

        # Function to replace each kanji match with an HTML link
        def replace_with_link(match):
            kanji = match.group(0)
            url = None

            if kanji in kanji_by_char:
                kd = kanji_by_char[kanji]
                url = kd["data"].get("document_url")

            if not url:
                url = "https://jisho.org/search/{{kanji}}%20%23kanji"
            return f'<a href="{url}">{kanji}</a>'

        linked_text = re.sub(kanji_regex, replace_with_link, text)

        return linked_text

    with open("extra-vocab.json") as f:
        print("Loading extra vocab data from extra-vocab.json")
        extra = json.load(f)

        for i, (v, d) in enumerate(extra.items()):
            vid = 200000 + i

            kanji_in_vocab = extract_kanji(v)

            if not kanji_in_vocab:
                print(f"No kanji in vocab {v}")

            components = [kanji_by_char[k]["id"] for k in kanji_in_vocab]

            wkv = {
                "id": vid,
                "object": "vocabulary",
                "data": {
                    "level": 999,
                    "characters": v,
                    "meanings": [
                        {"meaning": m, "primary": True} for m in d["meanings"]
                    ],
                    "readings": [
                        {"reading": r, "primary": True} for r in d["readings"]
                    ],
                    "component_subject_ids": components,
                },
            }

            all_vocab.append(wkv)

    kanji_freq = {}
    kanji_freq_tot = 0
    with open("freq-wikipedia.json") as f:
        print("Loading frequency data from freq-wikipedia.json")
        fd = json.load(f)
        for pos, (k, count, p) in enumerate(fd):
            if k == 'all':
                kanji_freq_tot = count
                continue
            kanji_freq[k] = (pos, count, p)

    # This should be equivalent to the "amalgamation_subject_ids" from the kanji
    # entry but WaniKani's bogus caching means that sometimes the vocabulary
    # entries are not correctly linked to the kanji, but the other ways around
    # (vocabulary referencing the kanji) seems to always work.
    vocab_for_kanji = {}
    # sentences = open("wk-sentences.md", "w")
    sentences = open("/dev/null", "w")
    sentences.write("# WaniKani example sentences\n")
    slevel = 0
    for v in all_vocab:
        for k in v["data"]["component_subject_ids"]:
            vocab_for_kanji.setdefault(k, list()).append(v)
        if v["data"]["level"] != slevel:
            slevel = v["data"]["level"]
            if slevel > 99:
                continue
            sentences.write(f"\n## Level {slevel}\n\n")

        if "context_sentences" not in v["data"]:
            continue

        sentences.write(f"- {v['data']['characters']}\n\n")
        for c in v["data"]["context_sentences"]:
            sentences.write(f"    + {c['ja']}\n")
            sentences.write(f"        - {c['en']}\n\n")


    now = datetime.datetime.now(datetime.UTC)
    gendate = now.strftime("%Y-%m-%d")

    freq_cover = 0

    for raw in all_kanji:
        kanji = raw["data"]
        char = kanji["characters"]
        url = kanji.get("document_url", "")
        on = [
            jaconv.hira2kata(r["reading"])
            for r in kanji["readings"]
            if r["type"] == "onyomi"
        ]
        kun = [r["reading"] for r in kanji["readings"] if r["type"] == "kunyomi"]
        nanori = [r["reading"] for r in kanji["readings"] if r["type"] == "nanori"]
        wk_meanings = [m["meaning"] for m in kanji["meanings"]]

        if char in kanjidic:
            k = kanjidic[char]
            kanjidic_meanings = [m for m in k["meanings"] if "(No." not in m]
            stroke_count = str(k["stroke_count"])
            jlpt = k["jlpt"]
            freq = k["freq"]
            radical = k["radical"]

            if radical == "":
                print("No radical for %s" % char)

            # Override wanikani data with kanjidic
            on = k["on"]
            kun = k["kun"]

            if char in kanji_freq:
                (pos, count, p) = kanji_freq[char]
                freq = f"Wikipedia: #{pos} ({(p * 100):.3f}%)"
                freq_cover += count

        else:
            print(f"No Kanjidic entry for {char}")
            kanjidic_meanings = []
            stroke_count = ""
            if char == "々":
                stroke_count = "3"
            jlpt = ""
            freq = ""
            radical = ""

        tags = []

        (
            keisei_table,
            kyuujitai,
            phonetic,
            semantic,
            phonetic_full,
            phonetic_partial,
            phonetic_nomatch,
        ) = get_keisei_data(char)

        if jlpt:
            tags.append(f"JLPT-N{jlpt}")

        merged_meanings = wk_meanings.copy()
        for m in kanjidic_meanings:
            if m not in merged_meanings:
                merged_meanings.append(m)

        svg = "{:05x}.svg".format(ord(char))
        svg_kyuujitai = None
        strokes = f'<img src="{svg}" alt="{char}">'
        # Some kyuujitai variants use an unicode variant selector (for instance
        # 勇󠄁) and KanjiVG doesn't support those
        if kyuujitai and len(kyuujitai) == 1:
            svg_kyuujitai = "{:05x}.svg".format(ord(kyuujitai))
            if os.path.exists(f"kanji_colored/{svg_kyuujitai}"):
                strokes += f"""
                    <details>
                        <summary>旧自体</summary>
                        <img src="{svg_kyuujitai}" alt="{kyuujitai}">
                    </details>"""
            else:
                print(f"No SVG for 旧自体 {kyuujitai}")
                svg_kyuujitai = None

        level = kanji["level"]
        syn = synonyms.get(char, [])
        similar_looking = [
            kanji_by_id[s]["data"]["characters"]
            for s in kanji["visually_similar_subject_ids"]
            if s in kanji_by_id
        ]
        vocab = vocab_for_kanji.get(raw["id"], [])
        if not vocab:
            print("No vocab for %s" % char)

        vocab = sorted(vocab, key=lambda v: v["data"]["level"])
        vocabulary = format_vocab(vocab, kanji=True, furigana=False)
        vocabulary_furigana = format_vocab(vocab, kanji=True, furigana=True)
        vocabulary_kana_only = format_vocab(vocab, kanji=False, furigana=False)
        vocabulary_cloze = vocabulary.replace(char, '<span class="kcloze">？</span>')
        vocabulary_clozefuri = vocabulary_furigana.replace(char, "？")

        note = KanjiNote(
            model=model,
            tags=tags,
            fields=[
                char,
                ", ".join(wk_meanings),
                ", ".join(kanjidic_meanings),
                ", ".join(merged_meanings),
                stroke_count,
                jlpt,
                freq,
                " ".join(["「" + r + "」" for r in on]),
                " ".join(["「" + r + "」" for r in kun]),
                " ".join(["「" + r + "」" for r in nanori]),
                str(level),
                strokes,
                radical,
                url,
                " ".join(jishoize_kanji(sorted(syn))),
                " ".join(jishoize_kanji(similar_looking)),
                keisei_table,
                vocabulary,
                vocabulary_furigana,
                vocabulary_kana_only,
                vocabulary_cloze,
                vocabulary_clozefuri,
                gendate,
            ],
        )
        deck.add_note(note)

        media_files.append("kanji_colored/" + svg)
        if svg_kyuujitai:
            media_files.append("kanji_colored/" + svg_kyuujitai)

        jd = {
            "kanjidic_meanings": kanjidic_meanings,
            "onyomi": on,
            "kunyomi": kun,
        }

        if kyuujitai:
            jd["kyuujitai"] = kyuujitai
        if phonetic:
            jd["phonetic_component"] = phonetic
        if semantic:
            jd["semantic_component"] = semantic
        if phonetic_full:
            jd["phonetic_series_perfect"] = phonetic_full
        if phonetic_partial:
            jd["phonetic_series_partial"] = phonetic_partial
        if phonetic_nomatch:
            jd["phonetic_series_nomatch"] = phonetic_nomatch
        json_dump[char] = jd

    package = genanki.Package(deck)
    package.media_files = media_files
    package.write_to_file("/tmp/kanji.apkg")

    with open("kanji_extra_data.json", "w") as f:
        json.dump(json_dump, f, ensure_ascii=False)

    print(f"Total coverage: {(freq_cover * 100) / kanji_freq_tot:.2f}%")
