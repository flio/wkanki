#!/usr/bin/env python
#
# I couldn't find a decent radical list anywhere so I just dump Wikipedia's
import json
import pandas

wiki_url = "https://en.wikipedia.org/wiki/List_of_kanji_radicals_by_stroke_count"


def load_radicals():
    table = pandas.read_html(wiki_url, index_col=0, header=0)[0]
    radicals = []
    for r in table.iterrows():
        radicals.append(
            {
                "number": int(r[0]),
                "radical": r[1][0],
            }
        )

    # Make sure that the table doesn't have holes or out-of-order radicals
    for i, r in enumerate(radicals):
        assert r["number"] == i + 1
    return radicals


if __name__ == "__main__":
    radicals = load_radicals()
    with open("radicals-kangxi.json", "w") as f:
        json.dump(radicals, f)
