#!/usr/bin/env python

from xml.dom import minidom
import json
import sys
import re

def jmnedict_parse(jmnedict_file):
    # Get latest jmnedict data at
    #    https://www.edrdg.org/wiki/index.php/JMdict-EDICT_Dictionary_Project
    print("Loading jmnedict data from %s" % jmnedict_file)
    dom = minidom.parse(jmnedict_file)

    entries = dom.getElementsByTagName("entry")

    r = []

    for e in entries:
        forms = [f.childNodes[0].data for f in e.getElementsByTagName("keb")]
        readings = [f.childNodes[0].data for f in e.getElementsByTagName("reb")]
        readings = [f.childNodes[0].data for f in e.getElementsByTagName("reb")]
        tye = e.getElementsByTagName('name_type')
        if tye:
            ty = tye[0].childNodes[0].data
        else:
            ty = "unclassified name"

        meanings = []
        for g in e.getElementsByTagName("trans_det"):
            meaning = g.childNodes[0].data
            meanings.append(meaning)

        r.append({
            "kanji_forms": forms,
            "readings": readings,
            "meanings": meanings,
            "type": ty.replace('&', '').replace(';', ''),
            })

    return r

def jmdict_parse(jmdict_file, freq_file):
    print("Loading frequency data from %s" % freq_file)

    freqs = {}

    skip_header = True

    with open(freq_file, 'r', encoding='utf-8') as file:
        for line in file:
            if skip_header:
                skip_header = False
                continue

            line = line.strip()
            [word, kana, api_freq, adjusted_freq, *rest] = line.split('\t')
            freq = int(adjusted_freq.strip())
            word = word.strip()
            kana = kana.strip()

            if word not in freqs:
                freqs[word] = freq
            if kana not in freqs:
                freqs[kana] = freq

    # Get latest jmdict data at
    #    https://www.edrdg.org/wiki/index.php/JMdict-EDICT_Dictionary_Project
    print("Loading jmdict data from %s" % jmdict_file)
    dom = minidom.parse(jmdict_file)

    entries = dom.getElementsByTagName("entry")

    r = []

    for e in entries:
        forms = [f.childNodes[0].data for f in e.getElementsByTagName("keb")]
        readings = [f.childNodes[0].data for f in e.getElementsByTagName("reb")]
        # Part-of-speech
        pos = [f.childNodes[0].data for f in e.getElementsByTagName("pos")]
        misc = [f.childNodes[0].data for f in e.getElementsByTagName("misc")]

        examples = []
        for ex in e.getElementsByTagName("example"):
            example = {}
            for sent in ex.getElementsByTagName("ex_sent"):
                lang = sent.getAttribute("xml:lang") or "eng"
                example[lang] = sent.childNodes[0].data
            examples.append(example)

        meanings = []
        for g in e.getElementsByTagName("gloss"):
            # meaning = g.childNodes[0].data.title()
            # meaning = meaning.replace("'T", "'t")
            # meaning = meaning.replace("'S", "'s")
            meaning = g.childNodes[0].data
            meanings.append(meaning)


        freq = 500000
        if forms and 'uk' not in misc:
            freq = min([ freqs.get(f, 500000) for f in forms], default=500000)

        if not forms or 'uk' in misc:
            freq = min([ freqs.get(f, freq) for f in readings], default=freq)

        r.append({
            "kanji_forms": forms,
            "readings": readings,
            "meanings": meanings,
            "examples": examples,
            "freq": freq,
            "pos": pos,
            "misc": misc,
            })

    def sort_key(d):
        if d['kanji_forms']:
            w = d['kanji_forms'][0]
        else:
            w = d['readings'][0]
        return (d['freq'], len(w), w)

    return sorted(r, key=sort_key)

def kradfile_parse(kradfile, kanjidic):

    print("Loading component data from %s" % kradfile)
    komponents = {}
    radicals  = {}

    # Kradfile sometimes uses characters in lieu of the radicals themselves due
    # to charset restrictions
    kradfile_remap = {
            "化": "⺅",
            "个": "𠆢",
            "并": "丷",
            "刈": "⺉",
            "込": "⻌",
            "尚": "⺌",
            "忙": "⺖",
            "扎": "⺘",
            "汁": "⺡",
            "犯": "⺨",
            "艾": "⺾",
            "邦": "⻏",
            "阡": "⻖",
            "老": "⺹",
            "杰": "⺣",
            "礼": "⺭",
            "疔": "⽧",
            "禹": "⽱",
            "初": "⻂",
            "買": "⺲",
            "滴": "啇",
            "乞": "𠂉",
            }

    extra_components = {
            '青': '龶',
            '麦': '龶',
            '鯖': '円冂亠一｜',
            '錆': '円冂亠一｜',
            '瀞': '円冂亠一｜',
            '倩': '円冂亠一｜',
            '猜': '円冂亠一｜',
            '睛': '円冂亠一｜',
            '菁': '円冂亠一｜',
            '蜻': '円冂亠一｜',
            '靜': '円冂亠一｜',
            '儬': '円冂亠一｜',
            '圊': '円冂亠一｜',
            '婧': '円冂亠一｜',
            '棈': '円冂亠一｜',
            '箐': '円冂亠一｜',
            '綪': '円冂亠一｜',
            '蒨': '円冂亠一｜',
            '蔳': '円冂亠一｜',
            '靕': '円冂亠一｜',
            '靗': '円冂亠一｜',
            '靘': '円冂亠一｜',
            '靚': '円冂亠一｜',
            '靛': '円冂亠一｜',
            '鼱': '円冂亠一｜',
            }

    # Stroke count for kanji not in kanjidic
    rad_strokes = {
            '｜': 1,
            '⻖': 3,
            '⺘': 3,
            '⻌': 3,
            '⺾': 3,
            'ノ': 1,
            '⺡': 3,
            '⺣': 4,
            '⻂': 5,
            'ヨ': 3,
            '⺅': 2,
            '⺖': 3,
            'ユ': 2,
            '丷': 2,
            'ハ': 2,
            '⻏': 3,
            '⺌': 3,
            '⺲': 5,
            '⺹': 4,
            '⽧': 5,
            '⺨': 3,
            'マ': 2,
            '⺭': 4,
            '⺉': 2,
            '⽱': 5,
            '龶': 4,
            }

    with open(kradfile, 'r', encoding='utf-8') as file:
        for line in file:
            line = line.strip().replace(' ', '')
            if (line[0] == '#'):
                continue
            [k, components] = line.split(":")

            # Ignore kanji that don't have a meaning in kanjidic
            kj = kanjidic[k]
            if not kj or not kj.get("meanings"):
                # Too obscure
                continue

            components = set(kradfile_remap.get(c, c) for c in components)

            for c, extra in extra_components.items():
                if c in components or c == k:
                    for e in extra:
                        components.add(e)

            for c in components:
                if c in radicals:
                    radicals[c]['count'] += 1
                else:
                    radicals[c] = {
                            'radical': c,
                            'count': 1,
                            }

                    if c in rad_strokes:
                        radicals[c]['strokes'] = rad_strokes[c]
                    else:
                        radicals[c]['strokes'] = kanjidic[c]['stroke_count']

            komponents[k] = sorted(components)

    with open("kradfile.json", "w") as f:
        json.dump(komponents, f, ensure_ascii=False)

    radicals = sorted(radicals.values(), key=lambda r: (r['strokes'], -r['count'], r['radical']))

    with open("radicals.json", "w") as f:
        json.dump(radicals, f, ensure_ascii=False)

    # Now create a lookup up table where every radicals and character is tied to
    # all possible radicals it can appear with.
    compatible_rads = {}
    def add_compatible_rad(rad, comp_with):
        s = compatible_rads.setdefault(comp_with, set())
        s.add(rad)

    for k, components in komponents.items():
        for c in components:
            add_compatible_rad(c, k);
            for o in components:
                if c != o:
                    add_compatible_rad(c, o)

    for k, c in compatible_rads.items():
        compatible_rads[k] = sorted(c)

    with open("radicalcompat.json", "w") as f:
        json.dump(compatible_rads, f, ensure_ascii=False)

    return komponents

def main():
    with open("kanjidic.json") as f:
        print("Loading Kanjidic data from kanjidict.json")
        kanjidic = json.load(f)

    # To generate kradfile.utf8:
    #
    # iconv -f euc-JP -t UTF-8 kradfile > kradfile.utf8
    # iconv -f euc-JP -t UTF-8 kradfile2 >> kradfile.utf8
    komponents = kradfile_parse('kradfile.utf8', kanjidic)

    # sed -E 's/<!ENTITY ([^ ]+) "[^"]*">/<!ENTITY \1 "\1">/' < JMdict_e_examp.xml > JMdict_e_examp-pp.xml
    jmdict = jmdict_parse("JMdict_e_examp-pp.xml", "jpdb_v2.1_freq_list.csv")

    print("Dumping jmdict.json")
    with open("jmdict.json", "w") as f:
        json.dump(jmdict, f, ensure_ascii=False, separators=(',', ':'))

    jmnedict = jmnedict_parse("JMnedict.xml")
    print("Dumping jmnedict.json")
    with open("jmnedict.json", "w") as f:
        json.dump(jmnedict, f, ensure_ascii=False, separators=(',', ':'))

if __name__ == "__main__":
    main()
