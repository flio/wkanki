"use strict";

(function (ksearch, undefined) {
    ksearch.verbose = (getURLParameter('v') == 1);
    ksearch.worker = new Worker('ksearch-worker.js');

    ksearch.worker_ready = false;
    ksearch.pending_pattern = undefined;

    ksearch.worker.onmessage = function (event) {
        let data = event.data;
        console.log("main rx", data);
        switch (data.action) {
        case "worker_ready":
            ksearch.worker_ready = true;
            maybe_start_search();
            break;
        case "search_results":
            display_search_results(data);
            break;
        case "search_done":
            maybe_start_search();
            break;
        default:
            console.error("Unknown action", data);
        }
    };

    function maybe_start_search() {
        if (!ksearch.worker_ready || ksearch.pending_pattern === undefined) {
            return;
        }

        ksearch.worker.postMessage({
                                   action: 'search',
                                   pattern: ksearch.pending_pattern,
                                   token: random_token(),
        })

        ksearch.pending_pattern = undefined;
    }

    let last_pattern = undefined;

    function getURLParameter(name) {
        const urlParams = new URLSearchParams(window.location.search);
        return urlParams.get(name);
    }

    function setURLParameter(name, value) {
        if (getURLParameter(name) === value) {
            return;
        }

        const url = new URL(window.location);
        url.searchParams.set(name, value);
        window.history.pushState({}, '', url);
    }

    function reformat_search_string(search_input) {
        let open_brackets = 0;
        let s = search_input.value;
        let ns = ''

        for (let i = 0; i < s.length; i++) {
            let c = s[i];
            if ('<〈＜「'.indexOf(c) >= 0) {
                open_brackets++;
                c = '「';
            } else if ('>〉＞」'.indexOf(c) >= 0) {
                open_brackets--;
                c = '」';
            } else if (c === '?') {
                c = '？';
            } else if (c === '*') {
                c = '＊';
            } else if (',、､'.indexOf(c) >= 0) {
                if (open_brackets > 0) {
                    c = '」';
                    open_brackets--;
                } else {
                    c = '「';
                    open_brackets++;
                }
            }

            ns += c;
        }

        if (ns != search_input.value && open_brackets == 0) {
            let start = search_input.selectionStart;
            let end = search_input.selectionEnd;
            search_input.value = ns;
            search_input.setSelectionRange(start, end);
        }
    }

    function fuzzy_search(search_input) {
        reformat_search_string(search_input);
        update_radical_picker();
        let pattern = search_input.value;

        if (pattern === last_pattern) {
            return;
        }

        setURLParameter('s', pattern);

        // pattern = pattern.replace(/\s/g, "");

        if (!pattern) {
            let results = document.getElementById('ksresults');
            results.innerHTML = '';
            return;
        }

        ksearch.pending_pattern = pattern;
        maybe_start_search();
    }

    function display_search_results(data) {
        let results = document.getElementById('ksresults');
        results.innerHTML = '';

        let matches = data.matches || [];

        const count_span = document.createElement('div');

        const result_count = document.createElement('div');
        result_count.classList.add('result-count');
        result_count.textContent = data.nmatches + " result(s) found";
        results.appendChild(result_count);

        // Limit max entries
        matches = matches.slice(0, 50);

        const ul = document.createElement('ul');

        matches.forEach(function (match) {
            let d = match.dict;

            const href = document.createElement('a');
            href.classList.add('match');

            if (d.kanji) {
                href.setAttribute('href', 'https://jisho.org/search/' + match.word + '%20%23kanji');
            } else {
                href.setAttribute('href', 'https://jisho.org/search/' + match.word);
            }

            const match_li = document.createElement('li');

            const word_span = document.createElement('span');
            word_span.classList.add('word');
            word_span.setAttribute('lang', 'ja');
            if (match.match_len) {
                let w = match.word;
                let o = match.match_offset;
                let l = match.match_len;

                let pre = w.slice(0, o);
                let m = w.slice(o, o + l);
                let post = w.slice(o + l);

                if (pre) {
                    const span = document.createElement('span');
                    span.classList.add('smatch-pre');
                    span.textContent = pre;
                    word_span.appendChild(span);
                }

                if (m) {
                    const span = document.createElement('span');
                    span.classList.add('smatch');
                    span.textContent = m;
                    word_span.appendChild(span);
                }

                if (post) {
                    const span = document.createElement('span');
                    span.classList.add('smatch-post');
                    span.textContent = post;
                    word_span.appendChild(span);
                }
            } else {
                word_span.textContent = match.word;
            }

            const def_span = document.createElement('div');
            def_span.classList.add('definition');
            def_span.setAttribute('lang', 'en');
            def_span.textContent = d.meanings.join('; ');

            const read_span = document.createElement('span');
            read_span.classList.add('reading');
            read_span.setAttribute('lang', 'ja');
            read_span.textContent = d.readings.join('、');


            match_li.appendChild(word_span);
            match_li.appendChild(read_span);
            if (d.kanji) {
                const kanji_span = document.createElement('span');
                kanji_span.classList.add('kanji');
                kanji_span.textContent = '漢字'
                kanji_span.setAttribute('lang', 'ja');
                match_li.appendChild(kanji_span);
            }
            if (match.freq < 100000) {
                const freq_span = document.createElement('span');
                freq_span.classList.add('freq');
                freq_span.textContent = 'Top ' + Math.ceil(match.freq / 1000) + 'k';
                match_li.appendChild(freq_span);
            }

            match_li.appendChild(def_span);
            href.appendChild(match_li);

            if (ksearch.verbose) {
                const div = document.createElement('div');
                div.classList.add('score');
                div.textContent = `Score: ${Math.round(match.score * 10)}`;
                match_li.appendChild(div);

                (match.captures || []).forEach(function (c) {
                    const div = document.createElement('div');
                    div.classList.add('capturelist');
                    div.setAttribute('lang', 'ja');

                    {
                        let span = document.createElement('span');
                        let l = Array.from(c.target).sort().join('');
                        span.textContent = `Matching ${c.candidate} against「${l}」: `
                        div.appendChild(span);
                    }

                    {
                        let span = document.createElement('span');
                        let l = Array.from(c.intersection).sort().join('');
                        span.textContent = `intersection: {${l}}, `
                        div.appendChild(span);
                    }

                    {
                        let span = document.createElement('span');
                        let l = Array.from(c.secondary_intersection).sort().join('');
                        span.textContent = `secondary_intersection: {${l}}, `
                        div.appendChild(span);
                    }

                    {
                        let span = document.createElement('span');
                        let l = Array.from(c.missing).sort().join('');
                        span.textContent = `missing: {${l}}, `
                        div.appendChild(span);
                    }

                    {
                        let span = document.createElement('span');
                        let l = Array.from(c.extra).sort().join('');
                        span.textContent = `extra: {${l}}`
                        div.appendChild(span);
                    }

                    match_li.appendChild(div);
                })
            }


            ul.appendChild(href);
        });

        results.appendChild(ul);
    }

    let radical_set = new Set();

    function init_radical_input(radicals, search_input) {
        let rlist = document.getElementById('radical-list');

        let cur_strokes = -1;

        function input_radical() {
            let radical = this.textContent;
            let selmode = this.getAttribute('selmode');
            let s = search_input.value;
            let selstart = search_input.selectionStart;
            let selend = search_input.selectionEnd;
            let open_brackets = 0;

            if (selmode == 'selected') {
                // Deselect the radical
                let rad_start = undefined;
                for (let i = selstart - 1; i >= 0; i--) {
                    if (s[i] == '」') {
                        break;
                    } else if (s[i] == '「') {
                        rad_start = i + 1;
                        break;
                    }
                }

                let rad_end = undefined;
                for (let i = selstart; i < s.length; i++) {
                    if (s[i] == '「') {
                        break;
                    } else if (s[i] == '」') {
                        rad_end = i;
                        break;
                    }
                }

                if (rad_start == undefined || rad_end == undefined) {
                    console.error("selected radical without block?", s, selstart);
                    return;
                }

                let selrads = new Set();

                for (let r of document.querySelectorAll('.radical-selector[selmode="selected"]')) {
                    if (r !== this) {
                        selrads.add(r.textContent);
                    }
                }

                let rads = Array.from(selrads).sort().join('');
                search_input.value = s.slice(0, rad_start) + rads + s.slice(rad_end);
                rad_end = rad_start + rads.length;
                search_input.setSelectionRange(rad_end, rad_end);
                console.log(rad_end);
            } else {
                for (let i = 0; i < selstart; i++) {
                    if (s[i] == '「') {
                        open_brackets++;
                    } else if (s[i] == '」') {
                        open_brackets--;
                    }
                }


                console.log(selstart, selend);

                let start = s.slice(0, selstart);
                let end = s.slice(selend);

                console.log(s, start, end);

                if (open_brackets <= 0) {
                    search_input.value = start + '「' + radical + '」' + end;
                    if (selend == selstart) {
                        selend += 2;
                    } else {
                        selend += 3;
                    }
                    selstart += 2;
                } else {
                    search_input.value = start + radical + end;
                    selstart += 1;
                    selend += 1;
                }

                search_input.setSelectionRange(selstart, selend);
            }

            setTimeout(function() {fuzzy_search(search_input)}, 0);
        }

        // Move ⻖ next to ⻏ for clarity
        {
            let from = radicals.findIndex(function (r) {
                return r.radical == '⻖';
            });

            let r = radicals.splice(from, 1)[0];

            let to = radicals.findIndex(function (r) {
                return r.radical == '⻏';
            });
            radicals.splice(to, 0, r);
        }

        radicals.forEach(function (r) {
            radical_set.add(r.radical);
            if (r.strokes !== cur_strokes) {
                let s = document.createElement('span');
                s.classList.add('radical-strokes');
                s.textContent = r.strokes;
                rlist.appendChild(s);
                cur_strokes = r.strokes;
            }

            let s = document.createElement('span');
            s.classList.add('radical-selector');
            s.id = radical_id(r.radical);
            if ('⻏⻖'.indexOf(r.radical) >= 0) {
                // These radicals look identical in the Japanese locale at least
                // on some devices (Firefox Windows for instance), switching to
                // chinese seems to fix it...
                s.setAttribute('lang', 'zh');
            }

            s.textContent = r.radical;
            rlist.appendChild(s);

            s.onclick = input_radical;
        });
    }

    let radcompat = {};

    async function init() {
        let search_input = document.getElementById('ksbar');
        search_input.value = getURLParameter('s') || "";

        let response = await fetch("radicals.json");
        if (!response.ok) {
            throw new Error("Can't load radicals.json: " + response.statusText)
        }
        let radicals = await response.json();

        response = await fetch("radicalcompat.json");
        if (!response.ok) {
            throw new Error("Can't load radicalcompat.json: " + response.statusText)
        }
        radcompat = await response.json();

        init_radical_input(radicals, search_input);

        search_input.addEventListener("input", function () {
            reformat_search_string(search_input);
            fuzzy_search(search_input);
        });

        search_input.addEventListener("change", function () {
            fuzzy_search(search_input);
        });

        window.addEventListener('popstate', function () {
            search_input.value = getURLParameter('s') || "";
            fuzzy_search(search_input);
        });

        search_input.addEventListener('keyup', update_radical_picker);
        search_input.addEventListener('click', update_radical_picker);

        let clear = document.getElementById('clear-button');
        clear.addEventListener('click', function () {
            search_input.value = "";
            search_input.focus();
            setTimeout(function() {fuzzy_search(search_input) }, 0);
        })

        setTimeout(function() {fuzzy_search(search_input) }, 0);
    }

    function update_radical_picker() {
        let search_input = document.getElementById('ksbar');

        let start = search_input.selectionStart;
        let end = search_input.selectionEnd;

        if (start != end) {
            // We have a selection active
            update_radical_selection('');
            return;
        }

        let s = search_input.value;

        // Find the brackets
        let rad_start = undefined;
        for (let i = start - 1; i >= 0; i--) {
            if (s[i] == '」') {
                break;
            } else if (s[i] == '「') {
                rad_start = i + 1;
                break;
            }
        }

        let rad_end = undefined;
        for (let i = start; i < s.length; i++) {
            if (s[i] == '「') {
                break;
            } else if (s[i] == '」') {
                rad_end = i;
                break;
            }
        }

        if (rad_start === undefined || rad_end === undefined) {
            update_radical_selection('');
            return;
        }

        let radicals = s.slice(rad_start, rad_end);
        update_radical_selection(radicals);
    }

    function update_radical_selection(radicals) {
        let all_radicals = document.querySelectorAll('.radical-selector');

        if (!radicals) {
            for (let rp of all_radicals) {
                rp.setAttribute('selmode', 'available');
            }
            return;
        }

        for (let rp of all_radicals) {
            rp.setAttribute('selmode', 'unavailable');
        }

        let used_rads = new Set();

        for (let r of radicals) {
            if (radical_set.has(r)) {
                used_rads.add(r);
                let rp = document.getElementById(radical_id(r));
                if (rp) {
                    rp.setAttribute('selmode', 'selected');
                }
            } else {
                // This is probably a non-radical kanji, consider its sub-parts
                // as selected
                for (let sr of radcompat[r] || []) {
                    used_rads.add(sr);
                    let rp = document.getElementById(radical_id(sr));
                    if (rp) {
                        rp.setAttribute('selmode', 'selected');
                    }
                }
            }
        }

        let compat_rads = undefined;

        for (let r of used_rads.values()) {
            let rads = radcompat[r];
            if (rads === undefined) {
                continue;
            }

            let s = new Set(rads)
            if (compat_rads === undefined) {
                compat_rads = s;
            } else {
                compat_rads = compat_rads.intersection(s);
            }
        }

        for (let r of compat_rads || []) {
            if (used_rads.has(r)) {
                continue;
            }
            let rp = document.getElementById(radical_id(r));
            if (rp) {
                rp.setAttribute('selmode', 'available');
            }
        }
    }

    function radical_id(r) {
        return 'rad-' + r.charCodeAt(0).toString(16);
    }

    init()

    function random_token() {
        return 'T#' + Math.random().toString(36).substr(2);
    }

})(window.ksearch = window.ksearch || {})

/* Polyfill for set intersection */
if (!Set.prototype.intersection) {
    Set.prototype.intersection = function(otherSet) {
        const out = new Set();
        for (const elem of otherSet) {
            if (this.has(elem)) {
                out.add(elem);
            }
        }
        return out;
    };
}

/* Dark/light mode handling */
document.addEventListener('DOMContentLoaded', () => {
    const toggleCheckbox = document.getElementById('theme-toggle');
    const currentTheme = localStorage.getItem('theme');

    const applyTheme = (theme) => {
        document.documentElement.setAttribute('data-theme', theme);
        localStorage.setItem('theme', theme);
        toggleCheckbox.checked = theme === 'light';
    };

    if (currentTheme) {
        applyTheme(currentTheme);
    } else {
        const prefersDarkScheme = window.matchMedia('(prefers-color-scheme: dark)').matches;
        applyTheme(prefersDarkScheme ? 'dark' : 'light');
    }

    toggleCheckbox.addEventListener('change', () => {
        const newTheme = toggleCheckbox.checked ? 'light' : 'dark';
        applyTheme(newTheme);
    });
});
