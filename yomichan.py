#!/usr/bin/env python3

import json
import datetime

with open("wk-vocab.json") as f:
    print("Loading vocabulary data from wk-vocab.json")
    all_vocab = json.load(f)

with open("wk-kanji.json") as f:
    print("Loading Kanji data from wk-kanji.json")
    all_kanji = json.load(f)

date = datetime.date.today()

index = {
    "title": "WK",
    "format": 3,
    "revision": date.strftime("WK-level-info_%Y-%m-%d"),
    "author": "Lionel Flandrin",
    "url": "https://gitlab.com/flio/wkanki",
    "description": "WaniKani level info for vocabulary entries",
}

term_meta = []

for v in all_vocab:
    data = v["data"]
    word = data["characters"]
    level = data["level"]
    entry = [
        word,
        "freq",
        level,
    ]
    term_meta.append(entry)

kanji_meta = []

for v in all_kanji:
    data = v["data"]
    word = data["characters"]
    level = data["level"]
    entry = [
        word,
        "freq",
        level,
    ]
    kanji_meta.append(entry)

with open("yomichan/index.json", "w") as f:
    json.dump(index, f, indent=2, ensure_ascii=False)

with open("yomichan/term_meta_bank_1.json", "w") as f:
    json.dump(term_meta, f, indent=2, ensure_ascii=False)

with open("yomichan/kanji_meta_bank_1.json", "w") as f:
    json.dump(kanji_meta, f, indent=2, ensure_ascii=False)
