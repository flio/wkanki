#!/usr/bin/env python

import json
import sqlite3
import os
import zlib

db_path = 'dict.db'

if os.path.exists(db_path):
    os.remove(db_path)

db = sqlite3.connect(db_path)
cursor = db.cursor()

cursor.execute('CREATE TABLE IF NOT EXISTS `goi` (id INTEGER PRIMARY KEY, entry BLOB)')
cursor.execute('CREATE TABLE IF NOT EXISTS `kanji` (id INTEGER PRIMARY KEY, kanji TEXT, entry BLOB)')
cursor.execute('CREATE TABLE IF NOT EXISTS `nanori` (id INTEGER PRIMARY KEY, entry BLOB)')

cursor.execute('CREATE INDEX IF NOT EXISTS idx_kanji ON kanji (kanji);')

def db_add(table, index, entry):
    e_json = json.dumps(entry)
    e_zson = zlib.compress(e_json.encode('utf-8'), level=9)

    if (table == 'kanji'):
        cursor.execute(f'INSERT INTO `{table}` (id, kanji, entry) VALUES (?, ?, ?)',
                       (index, entry['K'], e_zson))
    else:
        cursor.execute(f'INSERT INTO `{table}` (id, entry) VALUES (?, ?)',
                       (index, e_zson))

def dump_jmdict():
    with open("jmdict.json") as f:
        jmdict = json.load(f)

    dict = []

    for i, d in enumerate(jmdict):
        pd = {}

        def rename_copy_field(f, to):
            if f in d:
                d[to] = d.pop(f)
                pd[to] = d[to]

        rename_copy_field('kanji_forms', 'F')
        rename_copy_field('freq', 'f')
        rename_copy_field('meanings', 'M')
        rename_copy_field('readings', 'R')

        d['p'] = sorted(list(set(d.pop('pos'))))
        d['e'] = d.pop('examples')
        d['m'] = sorted(list(set(d.pop('misc'))))

        db_add('goi', i, d)

        if len(pd['M']) > 2:
            pd['M'] = pd['M'][:2]
            pd['M'].append('...')

        dict.append(pd)

    print("Dumping jmdict-fe-full.json")
    with open("jmdict-fe-full.json", "w") as f:
        json.dump(dict, f, ensure_ascii=False, separators=(',', ':'))

    dict = dict[0:60000]

    print("Dumping jmdict-fe-hf.json")
    with open("jmdict-fe-hf.json", "w") as f:
        json.dump(dict, f, ensure_ascii=False, separators=(',', ':'))

NAME_TYPE_TABLE = {
 "character": "char",
 "company name": "co",
 "creature": "creat",
 "deity": "god",
 "document": "doc",
 "event": "ev",
 "female given name or forename": "fem",
 "fiction": "fict",
 "given name or forename, gender not specified": "given",
 "group": "grp",
 "legend": "leg",
 "male given name or forename": "masc",
 "mythology": "myth",
 "object": "obj",
 "organization name": "org",
 "other": "oth",
 "full name of a particular person": "pers",
 "place name": "place",
 "product name": "product",
 "religion": "relig",
 "service": "serv",
 "ship name": "ship",
 "railway station": "eki",
 "family or surname": "fam",
 "unclassified name": "unclass",
 "work of art, literature, music, etc. name": "work",
 }

def dump_jmnedict():
    with open("jmnedict.json") as f:
        jmnedict = json.load(f)

    # For now let's ignore the kana-only names to save some space
    jmnedict = [ e for e in jmnedict if e['kanji_forms'] ]

    dict = []

    for i, d in enumerate(jmnedict):
        pd = {}

        if 'type' in d:
            d['type'] = NAME_TYPE_TABLE[d['type']]

        def rename_copy_field(f, to):
            if f in d:
                d[to] = d.pop(f)
                pd[to] = d[to]

        rename_copy_field('kanji_forms', 'F')
        rename_copy_field('meanings', 'M')
        rename_copy_field('readings', 'R')
        rename_copy_field('type', 'c')


        db_add('nanori', i, d)

        pd.pop('M')

        dict.append(pd)

    print("Dumping jmnedict-fe.json")
    with open("jmnedict-fe.json", "w") as f:
        json.dump(dict, f, ensure_ascii=False, separators=(',', ':'))


def dump_kanjidic():
    with open("kanjidic.json") as f:
        kanjidic = json.load(f)

    dict = []

    for i, (k, d) in enumerate([ (k, d) for (k, d) in kanjidic.items() if d.get('meanings')]):

        pd = { 'K': k }

        def rename_copy_field(f, to):
            if f in d:
                d[to] = d.pop(f)
                pd[to] = d[to]

        rename_copy_field('kanji_forms', 'F')
        rename_copy_field('on', 'R')
        rename_copy_field('meanings', 'M')

        if d.get('freq'):
            d['f'] = int(d.pop('freq'))
        else:
            d['f'] = 500000

        pd['f'] = d['f']

        db_entry = {
                'K': k,
                'M': pd['M'],
                'R': pd['R'],
                'kun': d['kun'],
                'M': pd['M'],
                'f': pd['f'],
                }

        svg_path = "kanji_colored/{:05x}.svg".format(ord(k))
        try:
            with open(svg_path, 'r', encoding='utf-8') as f:
                db_entry['svg'] = f.read()
        except FileNotFoundError:
            pass


        db_add('kanji', i, db_entry)

        if len(pd['M']) > 2:
            pd['M'] = pd['M'][:2]
            pd['M'].append('...')

        dict.append(pd)

    print("Dumping kanjidic-fe.json")
    with open("kanjidic-fe.json", "w") as f:
        json.dump(dict, f, ensure_ascii=False, separators=(',', ':'))

dump_kanjidic()
dump_jmdict()
dump_jmnedict()

db.commit()
db.close()

